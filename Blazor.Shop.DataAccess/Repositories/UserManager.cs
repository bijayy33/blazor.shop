﻿
using System;
using System.Collections.Generic;

using Blazor.Shop.DependencyInjection.Models;
using Blazor.Shop.DependencyInjection.Repositories;

using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Blazor.Shop.DataAccess.Repositories
{
	public class UserManager : UserManager<ShopUser>, IUserManager
	{
		public UserManager(IUserStore<ShopUser> store,
			IOptions<IdentityOptions> optionsAccessor,
			IPasswordHasher<ShopUser> passwordHasher,
			IEnumerable<IUserValidator<ShopUser>> userValidators,
			IEnumerable<IPasswordValidator<ShopUser>> passwordValidators,
			ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors,
			IServiceProvider services,
			ILogger<UserManager<ShopUser>> logger)
			: base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
		{ }
	}
}
