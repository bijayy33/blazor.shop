﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.DataAccess.Context;
using Blazor.Shop.DependencyInjection;
using Blazor.Shop.DependencyInjection.Models;
using Blazor.Shop.DependencyInjection.Repositories;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

namespace Blazor.Shop.DataAccess.Repositories
{
	public class GenericRepository : IGenericRepository
	{
		private readonly ShopDbContext _context;
		private readonly IConfiguration _configuration;
		private readonly IHttpContextAccessor _httpContext;
		private readonly ILogger<GenericRepository> _logger;
		private readonly UserManager<ShopUser> _userManager;

		public GenericRepository(ShopDbContext context,
			IConfiguration configuration,
			IHttpContextAccessor httpContext,
			ILogger<GenericRepository> logger,
			UserManager<ShopUser> userManager)
		{
			this._context = context;
			this._configuration = configuration;
			this._httpContext = httpContext;
			this._logger = logger;
			this._userManager = userManager;
		}

		public AppDbContext Context => this._context;

		public IQueryable<TEntity> Table<TEntity>() where TEntity : class, new()
		=> this._context.Set<TEntity>();

		public async Task<TEntity> GetAsync<TEntity>(string id, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new()
		{
			if (id == null)
			{
				throw new ArgumentNullException("id");
			}

			return await this.Execute(async () => await this._context.Set<TEntity>().FirstOrDefaultAsync(p => p.Id == id, cancellationToken) ?? new TEntity(),
				$"[{nameof(GenericRepository)}][{nameof(GetAsync)}] Error occured while getting record by id: {id}.");
		}

		public async Task<TEntity> GetAsync<TEntity>(Expression<Func<TEntity, bool>> expression, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new()
		{
			if (expression == null)
			{
				throw new ArgumentNullException("expression");
			}

			return await this.Execute(async () => await this._context.Set<TEntity>().FirstOrDefaultAsync(expression, cancellationToken) ?? new TEntity(),
				$"[{nameof(GenericRepository)}][{nameof(GetAsync)}] Error occured while getting record by expression: {expression}.");
		}

		public async Task<IEnumerable<TEntity>> GetsAsync<TEntity>(CancellationToken cancellationToken = default, Expression<Func<TEntity, bool>> expression = null) where TEntity : BaseEntity, new()
		{
			return await this.Execute(async () =>
			{
				if (expression != null)
				{
					return await this._context.Set<TEntity>().Where(expression).ToListAsync(cancellationToken);
				}

				return await this._context.Set<TEntity>().ToListAsync(cancellationToken);
			},
			$"[{nameof(GenericRepository)}][{nameof(GetsAsync)}] Error occured while getting records by expression: {expression}.");
		}

		public async Task<TEntity> AddAsync<TEntity>(TEntity entity, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new()
		{
			if (entity == null)
			{
				throw new ArgumentNullException("entity");
			}

			return await this.Execute(async () =>
			{
				var user = await this._userManager.GetUserAsync(this._httpContext.HttpContext.User);
				entity.CreatedById = user?.Id;
				entity.CreationDate = DateTimeOffset.Now;

				await this._context.AddAsync<TEntity>(entity);
				await this._context.SaveChangesAsync(cancellationToken);
				return entity;
			},
			$"[GenericRepository][Add] Error occured for entity: {JsonConvert.SerializeObject(entity)}."
			);
		}

		public async Task<IEnumerable<TEntity>> AddMultipleAsync<TEntity>(List<TEntity> entities, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new()
		{
			if (entities == null)
			{
				throw new ArgumentNullException("entities");
			}

			return await this.Execute(async () =>
			{
				if (this._httpContext.HttpContext.User != null)
				{
					var user = await this._userManager.GetUserAsync(this._httpContext.HttpContext.User);
					DateTimeOffset today = DateTimeOffset.Now;

					for (int i = 0; i < entities.Count(); i++)
					{
						entities[i].CreatedById = user.Id;
						entities[i].CreationDate = today;
					}
				}

				await this._context.AddRangeAsync(entities, cancellationToken);
				await this._context.SaveChangesAsync();
				return entities;
			},
			$"[GenericRepository][AddMultiple] Error occured for entities: {JsonConvert.SerializeObject(entities)}."
			);
		}

		public async Task<TEntity> UpdateAsync<TEntity>(string id, TEntity entity, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new()
		{
			if (id == null)
			{
				throw new ArgumentNullException("id");
			}

			if (entity == null)
			{
				throw new ArgumentNullException("entity");
			}

			return await this.Execute(async () =>
			{
				var user = await this._userManager.GetUserAsync(this._httpContext.HttpContext.User);

				entity.ChangedById = user?.Id;
				entity.ChangedDate = DateTimeOffset.Now;

				this._context.Update<TEntity>(entity);
				await this._context.SaveChangesAsync(cancellationToken);
				return entity;
			},
			$"[GenericRepository][Update] Error occured for id: {id}."
			);
		}

		public async Task<IEnumerable<TEntity>> UpdateMultipleAsync<TEntity>(List<TEntity> entities, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new()
		{
			if (entities == null || !entities.Any())
			{
				throw new ArgumentNullException("entities");
			}

			return await this.Execute(async () =>
			{
				var user = await this._userManager.GetUserAsync(this._httpContext.HttpContext.User);

				for (int i = 0; i < entities.Count(); i++)
				{
					entities[i].ChangedById = user?.Id;
					entities[i].ChangedDate = DateTimeOffset.Now;
				}

				this._context.UpdateRange(entities);
				await this._context.SaveChangesAsync(cancellationToken);
				return entities;
			},
			$"[GenericRepository][UpdateMultipleAsync] Error occured for entities: {JsonConvert.SerializeObject(entities)}."
			);
		}

		public async Task<TEntity> DeleteAsync<TEntity>(string id, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new()
		{
			if (id == null)
			{
				throw new ArgumentNullException("id");
			}

			TEntity tempEntity = await this.GetAsync<TEntity>(id);

			if (tempEntity == null)
			{
				this._logger.LogError($"[GenericRepository][Delete] Record does not exists for id: {id}.");
				throw new InvalidOperationException("Error occured while deleting. Please check log for more information.");
			}

			if (_configuration.GetSection("IsHardDelete")?.Value != "True")
			{
				tempEntity.IsDeleted = true;
				return await this.UpdateAsync(tempEntity.Id, tempEntity, cancellationToken);
			}

			return await this.Execute(async () =>
			{
				//Add to shop log table --> Do this in shop service.
				this._context.Remove<TEntity>(tempEntity);
				await this._context.SaveChangesAsync(cancellationToken);
				return tempEntity;
			},
			$"[GenericRepository][Delete] Error occured for id: {id}."
			);
		}
		public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
		{
			return await this.Execute(async () => await this._context.SaveChangesAsync(cancellationToken),
				"Error occured while explicitly saving the changes to the database.");
		}

		public void Dispose()
		{
			GC.SuppressFinalize(this);
			this._logger.LogInformation($"[GenericRepository][Dispose] object disposed : {this}.");
		}

		#region Helper Methods

		private async Task<TResult> Execute<TResult>(Func<Task<TResult>> func, string errorMessage)
		{
			try
			{
				return await func();
			}
			catch (DbException ex)
			{
				this._logger.LogError($"DbException - { errorMessage }", ex);
			}
			catch (DbUpdateConcurrencyException ex)
			{
				this._logger.LogError($"DbUpdateConcurrencyException - { errorMessage }", ex);
			}
			catch (DbUpdateException ex)
			{
				this._logger.LogError($"DbUpdateException - { errorMessage }", ex);
			}
			catch (Exception ex)
			{
				this._logger.LogError($"Exception - { errorMessage }", ex);
			}

			return default;
		}

		#endregion Helper Methods
	}
}
