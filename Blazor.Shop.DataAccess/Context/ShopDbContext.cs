﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;

using Blazor.Shop.DependencyInjection;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Configuration;

namespace Blazor.Shop.DataAccess.Context
{
	public class ShopDbContext : AppDbContext
	{
		public ShopDbContext() : base()
		{
		}

		public ShopDbContext(DbContextOptions<ShopDbContext> options) : base(options)
		{
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			if (!optionsBuilder.IsConfigured)
			{
				IConfigurationRoot configuration = new ConfigurationBuilder()
						.SetBasePath(Directory.GetCurrentDirectory())
						.AddJsonFile("appsettings.json")
						.Build();
				string connectionString = configuration.GetConnectionString("DefaultConnection");

				if (connectionString.Contains("IsSqlite=true"))
				{
					optionsBuilder.UseSqlite(connectionString.Split(';')[0].Trim());
				}
				else
				{
					optionsBuilder.UseMySql(connectionString);
				}
			}

			optionsBuilder.EnableSensitiveDataLogging();
			base.OnConfiguring(optionsBuilder);
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			foreach (Assembly assembly in AssemblyLoadContext.Default.Assemblies)
			{
				builder.ApplyConfigurationsFromAssembly(assembly);
			}

			base.OnModelCreating(builder);
		}
	}
}
