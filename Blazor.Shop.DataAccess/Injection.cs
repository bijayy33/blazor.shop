﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using AutoMapper;

using Blazor.Shop.DataAccess.Context;
using Blazor.Shop.DataAccess.Repositories;
using Blazor.Shop.DependencyInjection;
using Blazor.Shop.DependencyInjection.AutoMappers;
using Blazor.Shop.DependencyInjection.Models;
using Blazor.Shop.DependencyInjection.Repositories;
using Blazor.Shop.DependencyInjection.Services;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Blazor.Shop.DataAccess
{
	public class Injection : IInjection
	{
		public Task Inject(IServiceCollection services, IConfiguration configuration)
		{
			services.AddScoped<AppDbContext, ShopDbContext>();

			services.AddDbContext<ShopDbContext>(optionBuilder =>
			{
				string connectionString = configuration.GetConnectionString("DefaultConnection");

				if (connectionString.Contains("IsSqlite=true"))
				{
					optionBuilder.UseSqlite(connectionString.Split(';')[0].Trim());
				}
				else
				{
					optionBuilder.UseMySql(connectionString, (options) =>
					{
						//options.MigrationsAssembly("Blazor.Shop.Api");
						options.EnableRetryOnFailure(3, TimeSpan.FromSeconds(2), new List<int> { 1, 2, 3 });
					});
				}
			});

			services.AddIdentity<ShopUser, IdentityRole>(option =>
			{
				option.SignIn.RequireConfirmedAccount = true;
			})
			.AddEntityFrameworkStores<ShopDbContext>()
			.AddDefaultTokenProviders();

			services.AddScoped<IGenericRepository, GenericRepository>();
			//services.AddSingleton<IUserManager, UserManager>();

			//services.AddSingleton(provider => new MapperConfiguration(cfg =>
			//{
			//	cfg.AddProfile(new MapProfile(provider.GetService<IShopService>()));
			//}).CreateMapper());
			return Task.CompletedTask;
		}
	}
}
