﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Blazor.Shop.DataAccess.Migrations
{
    public partial class Initial_Create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    DefaultLanguage = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Company",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedById = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 836, DateTimeKind.Unspecified).AddTicks(5249), new TimeSpan(0, 5, 45, 0, 0))),
                    ChangedById = table.Column<string>(nullable: true),
                    ChangedDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 841, DateTimeKind.Unspecified).AddTicks(4913), new TimeSpan(0, 5, 45, 0, 0))),
                    Name = table.Column<string>(nullable: true),
                    TaxTypeNumber = table.Column<string>(nullable: true),
                    TaxType = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Emails = table.Column<string>(nullable: true),
                    Phones = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FileModel",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedById = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 849, DateTimeKind.Unspecified).AddTicks(2201), new TimeSpan(0, 5, 45, 0, 0))),
                    ChangedById = table.Column<string>(nullable: true),
                    ChangedDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 849, DateTimeKind.Unspecified).AddTicks(2861), new TimeSpan(0, 5, 45, 0, 0))),
                    Name = table.Column<string>(nullable: true),
                    Base64String = table.Column<string>(nullable: true),
                    Bytes = table.Column<byte[]>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    For = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileModel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedById = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 894, DateTimeKind.Unspecified).AddTicks(4550), new TimeSpan(0, 5, 45, 0, 0))),
                    ChangedById = table.Column<string>(nullable: true),
                    ChangedDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 894, DateTimeKind.Unspecified).AddTicks(5219), new TimeSpan(0, 5, 45, 0, 0))),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Size = table.Column<string>(nullable: true),
                    EncodedMrp = table.Column<string>(nullable: true),
                    EncodedCostPrice = table.Column<string>(nullable: true),
                    EncodedSellingPrice = table.Column<string>(nullable: true),
                    AvailableQuantity = table.Column<int>(nullable: false),
                    QuantityType = table.Column<int>(nullable: false),
                    QuantityTypeValue = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShopLog",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedById = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 902, DateTimeKind.Unspecified).AddTicks(1524), new TimeSpan(0, 5, 45, 0, 0))),
                    ChangedById = table.Column<string>(nullable: true),
                    ChangedDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 902, DateTimeKind.Unspecified).AddTicks(3012), new TimeSpan(0, 5, 45, 0, 0))),
                    Details = table.Column<string>(nullable: true),
                    For = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShopLog", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SymbolicPrice",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedById = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 916, DateTimeKind.Unspecified).AddTicks(7622), new TimeSpan(0, 5, 45, 0, 0))),
                    ChangedById = table.Column<string>(nullable: true),
                    ChangedDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 916, DateTimeKind.Unspecified).AddTicks(8278), new TimeSpan(0, 5, 45, 0, 0))),
                    FiscalYear = table.Column<string>(nullable: true),
                    Alphabet = table.Column<string>(nullable: false),
                    Value = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SymbolicPrice", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Vat",
                columns: table => new
                {
                    InvoiceNumber = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedById = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 928, DateTimeKind.Unspecified).AddTicks(7735), new TimeSpan(0, 5, 45, 0, 0))),
                    ChangedById = table.Column<string>(nullable: true),
                    ChangedDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 928, DateTimeKind.Unspecified).AddTicks(8354), new TimeSpan(0, 5, 45, 0, 0))),
                    VatPercentage = table.Column<decimal>(nullable: false),
                    TotalTaxableAmount = table.Column<decimal>(nullable: false),
                    TotalVatAmount = table.Column<decimal>(nullable: false),
                    TotalAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vat", x => x.InvoiceNumber);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sold",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedById = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 903, DateTimeKind.Unspecified).AddTicks(6178), new TimeSpan(0, 5, 45, 0, 0))),
                    ChangedById = table.Column<string>(nullable: true),
                    ChangedDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 903, DateTimeKind.Unspecified).AddTicks(6951), new TimeSpan(0, 5, 45, 0, 0))),
                    SellerId = table.Column<string>(nullable: true),
                    BuyerId = table.Column<string>(nullable: true),
                    TotalAmount = table.Column<decimal>(nullable: false),
                    TotalPaidAmount = table.Column<decimal>(nullable: false),
                    TotalDiscountAmount = table.Column<decimal>(nullable: false),
                    TotalPendingAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sold", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sold_AspNetUsers_BuyerId",
                        column: x => x.BuyerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sold_AspNetUsers_SellerId",
                        column: x => x.SellerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CompanyVat",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedById = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 842, DateTimeKind.Unspecified).AddTicks(4162), new TimeSpan(0, 5, 45, 0, 0))),
                    ChangedById = table.Column<string>(nullable: true),
                    ChangedDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 842, DateTimeKind.Unspecified).AddTicks(4819), new TimeSpan(0, 5, 45, 0, 0))),
                    BillNumber = table.Column<string>(nullable: true),
                    VatPercentage = table.Column<decimal>(nullable: false),
                    PurchasedAmount = table.Column<decimal>(nullable: false),
                    TaxAmount = table.Column<decimal>(nullable: false),
                    AmountWithoutTax = table.Column<decimal>(nullable: false),
                    GroupName = table.Column<string>(nullable: true),
                    CompanyId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyVat", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanyVat_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductVat",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedById = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 895, DateTimeKind.Unspecified).AddTicks(3742), new TimeSpan(0, 5, 45, 0, 0))),
                    ChangedById = table.Column<string>(nullable: true),
                    ChangedDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 895, DateTimeKind.Unspecified).AddTicks(4389), new TimeSpan(0, 5, 45, 0, 0))),
                    Quantity = table.Column<int>(nullable: false),
                    QuantityType = table.Column<int>(nullable: false),
                    Rate = table.Column<decimal>(nullable: false),
                    DiscountAmount = table.Column<decimal>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    InvoiceNumber = table.Column<long>(nullable: false),
                    VatInvoiceNumber = table.Column<long>(nullable: true),
                    ProductId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductVat", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductVat_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductVat_Vat_VatInvoiceNumber",
                        column: x => x.VatInvoiceNumber,
                        principalTable: "Vat",
                        principalColumn: "InvoiceNumber",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SoldLog",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedById = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 904, DateTimeKind.Unspecified).AddTicks(7399), new TimeSpan(0, 5, 45, 0, 0))),
                    ChangedById = table.Column<string>(nullable: true),
                    ChangedDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 904, DateTimeKind.Unspecified).AddTicks(8117), new TimeSpan(0, 5, 45, 0, 0))),
                    TotalAmount = table.Column<decimal>(nullable: false),
                    TotalPaidAmount = table.Column<decimal>(nullable: false),
                    TotalDiscountAmount = table.Column<decimal>(nullable: false),
                    TotalPendingAmount = table.Column<decimal>(nullable: false),
                    SoldId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SoldLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SoldLog_Sold_SoldId",
                        column: x => x.SoldId,
                        principalTable: "Sold",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SoldProduct",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedById = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 905, DateTimeKind.Unspecified).AddTicks(5995), new TimeSpan(0, 5, 45, 0, 0))),
                    ChangedById = table.Column<string>(nullable: true),
                    ChangedDate = table.Column<DateTimeOffset>(nullable: true, defaultValue: new DateTimeOffset(new DateTime(2020, 7, 7, 18, 39, 13, 905, DateTimeKind.Unspecified).AddTicks(6757), new TimeSpan(0, 5, 45, 0, 0))),
                    AvailableQuantity = table.Column<int>(nullable: false),
                    SoldQuantity = table.Column<int>(nullable: false),
                    QuantityType = table.Column<int>(nullable: false),
                    Rate = table.Column<decimal>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    DiscountAmount = table.Column<decimal>(nullable: false),
                    SoldId = table.Column<string>(nullable: true),
                    ProductId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SoldProduct", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SoldProduct_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SoldProduct_Sold_SoldId",
                        column: x => x.SoldId,
                        principalTable: "Sold",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CompanyVat_CompanyId",
                table: "CompanyVat",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductVat_ProductId",
                table: "ProductVat",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductVat_VatInvoiceNumber",
                table: "ProductVat",
                column: "VatInvoiceNumber");

            migrationBuilder.CreateIndex(
                name: "IX_Sold_BuyerId",
                table: "Sold",
                column: "BuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_Sold_SellerId",
                table: "Sold",
                column: "SellerId");

            migrationBuilder.CreateIndex(
                name: "IX_SoldLog_SoldId",
                table: "SoldLog",
                column: "SoldId");

            migrationBuilder.CreateIndex(
                name: "IX_SoldProduct_ProductId",
                table: "SoldProduct",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_SoldProduct_SoldId",
                table: "SoldProduct",
                column: "SoldId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CompanyVat");

            migrationBuilder.DropTable(
                name: "FileModel");

            migrationBuilder.DropTable(
                name: "ProductVat");

            migrationBuilder.DropTable(
                name: "ShopLog");

            migrationBuilder.DropTable(
                name: "SoldLog");

            migrationBuilder.DropTable(
                name: "SoldProduct");

            migrationBuilder.DropTable(
                name: "SymbolicPrice");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Company");

            migrationBuilder.DropTable(
                name: "Vat");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "Sold");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
