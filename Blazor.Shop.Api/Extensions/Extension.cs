﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;

using Blazor.Shop.DependencyInjection;
using Blazor.Shop.DependencyInjection.Common.Constants;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;

using NLog.Web;

namespace Blazor.Shop.Api.Extensions
{
	public static class Extension
	{
		private static IWebHostEnvironment environment;
		private static IConfiguration configuration;

		/// <summary>
		/// Inject the required project to the API Project.
		/// </summary>
		/// <param name="services"></param>
		/// <param name="env"></param>
		/// <param name="config"></param>
		public static void AddDependencyInjection(this IServiceCollection services, IWebHostEnvironment env, IConfiguration config)
		{
			var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

			try
			{
				environment = env;
				configuration = config;

				foreach (Type type in GetImplementations<IInjection>())
				{
					//Instansiate the class
					IInjection injection = (IInjection)Activator.CreateInstance(type);

					//Inject the particular services or required dependencies to the API project
					injection.Inject(services, configuration);
				}
			}
			catch (FileNotFoundException ex)
			{
				//File Handling Exception
				logger.Error("Error occured in [Extension][DependencyInjection] - File Not Found.", ex);
			}
			catch (FileLoadException ex)
			{
				//File Handling Exception when file found but failed to load
				logger.Error("Error occured in [Extension][DependencyInjection] - File Found But Failed To Load.", ex);
			}
			catch (Exception ex)
			{
				//Internal Server Error
				logger.Error<Exception>("Error occured in [Extension][DependencyInjection] - Internal Server Error.", ex);
			}
		}

		public static IEnumerable<Type> GetImplementations<IInterface>()
		{
			IEnumerable<Assembly> assemblies = LoadAssemblies();

			//var assembles = AssemblyLoadContext.Default.Assemblies;
			IEnumerable<Type> implementations = assemblies.SelectMany(assembly => assembly.GetTypes())
				.Where(type => typeof(IInterface).IsAssignableFrom(type)
											 && !type.IsInterface
											 && !type.IsAbstract)
				.ToList();

			return implementations;
		}

		public static IEnumerable<Assembly> LoadAssemblies()
		{
			string configPath = configuration.GetSection("DllPath")?.Value;

			if (!string.IsNullOrWhiteSpace(configPath))
			{
				DirectoryInfo dllPath = new DirectoryInfo(configPath);

				IEnumerable<FileInfo> files = dllPath.GetFiles().Where(file => file.Extension == ".dll");

				foreach (FileInfo file in files)
				{
					yield return AssemblyLoadContext.Default.LoadFromAssemblyPath(file.FullName);
				}
			}
			else
			{
				DirectoryInfo directoryInfo = new DirectoryInfo("../");
				DirectoryInfo[] directories = directoryInfo.GetDirectories();

				foreach (DirectoryInfo directory in directories)
				{
					string path = string.Empty;
					string directoryName = directory.FullName.Split('\\').Last();

					if (environment.IsDevelopment())
					{
						path = directory.FullName + @"\bin\debug\netcoreapp3.1";
					}
					else
					{
						path = directory.FullName + @"\bin\release\netcoreapp3.1";
					}

					DirectoryInfo dllPath = new DirectoryInfo(path);

					if (dllPath.Exists)
					{
						yield return AssemblyLoadContext.Default.LoadFromAssemblyPath(@$"{path}\{directoryName}.dll");
					}
				}
			}
		}

		public static void AddApplicationAuthentication(this IServiceCollection services, IConfiguration config)
		{
			services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
			 .AddJwtBearer(jwtBearerOptions =>
			 {
				 jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
				 {
					 ValidateIssuer = true,
					 ValidateAudience = true,
					 ValidateLifetime = false,
					 ValidateIssuerSigningKey = true,
					 ValidIssuer = config.GetSection("JWT:Issuer").Value,
					 ValidAudience = config.GetSection("JWT:Audience").Value,
					 IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(config.GetSection("JWT:SigningKey").Value))
				 };
			 });
		}

		public static void AddClaimBasedAuthorization(this IServiceCollection services)
		{
			services.AddAuthorization(options =>
			{
				options.AddPolicy(DefaultRole.SuperAdmin, policy => policy.RequireRole(DefaultRole.SuperAdmin));
				options.AddPolicy(DefaultRole.Seller, policy => policy.RequireRole(DefaultRole.Seller));
				options.AddPolicy(DefaultRole.Seller, policy => policy.RequireRole(DefaultRole.Seller));
			});
		}
	}
}
