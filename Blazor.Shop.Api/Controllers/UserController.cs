﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using Blazor.Shop.DependencyInjection.Common.Constants;
using Blazor.Shop.DependencyInjection.Commons.Constants;
using Blazor.Shop.DependencyInjection.Commons.Enums;
using Blazor.Shop.DependencyInjection.Models;
using Blazor.Shop.DependencyInjection.Models.Dtos;
using Blazor.Shop.DependencyInjection.Repositories;
using Blazor.Shop.DependencyInjection.Services;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

namespace Blazor.Shop.Api.Controllers
{
	//[Authorize(Roles = "SuperAdmin,Admin,SubAdmin")] //This OR conditionm, URL - https://docs.microsoft.com/en-us/aspnet/core/security/authorization/roles?view=aspnetcore-3.1
	//[Authorize(Roles = DefaultRole.Admin)]
	//[Authorize(Roles = DefaultRole.SubAdmin)] //These there are AND conditions
	//[Authorize(policy: = DefaultRole.SubAdmin)]
	//[Authorize] //You can also lock down a controller but allow anonymous, unauthenticated access to individual actions.
	[Route("api/{controller}")]
	[ApiController]
	public class UserController : ControllerBase
	{
		#region Field Or Variables

		private readonly string _accessToken;
		private readonly SignInManager<ShopUser> _signInManager;
		private readonly RoleManager<IdentityRole> _roleManager;
		private readonly UserManager<ShopUser> _userManager;
		private readonly IAuthService _authService;
		private readonly IGenericRepository _repository;
		private readonly ILogger<UserController> _logger;
		private readonly IConfiguration _configuration;
		private readonly IMapper _mapper;

		#endregion Field Or Variables

		#region Constructors

		public UserController(
			SignInManager<ShopUser> signInManager,
			RoleManager<IdentityRole> roleManager,
			UserManager<ShopUser> userManager,
			IAuthService authService,
			IGenericRepository repository,
			ILogger<UserController> logger,
			IConfiguration configuration,
			IMapper mapper)
		{
			this._signInManager = signInManager;
			this._userManager = userManager;
			this._authService = authService;
			this._repository = repository;
			this._logger = logger;
			this._configuration = configuration;
			this._mapper = mapper;
			this._accessToken = authService.GetJwtToken();
			_roleManager = roleManager;
		}

		#endregion Constructors

		#region User Sign

		[AllowAnonymous]
		[HttpPost("login")]
		public async Task<IActionResult> LogInUser(UserSignInDto user, CancellationToken cancellationToken = default)
		{
			try
			{
				bool isHardDelete = _configuration.GetSection("IsHardDelete").Value != "True";
				ShopUser shopUser = await this.GetUserAsync(user.UserName.ToUpperInvariant(), cancellationToken);

				var result = await this._signInManager.PasswordSignInAsync(shopUser, user.Password, true, false);

				if (result.Succeeded)
				{
					await this.SignInAsync(shopUser);

					return Ok(this._mapper.Map<UserDto>(shopUser));
				}
			}
			catch (Exception ex)
			{
				this._logger.LogError($"[{nameof(UserController)}][{(nameof(LogInUser))}] Error occured.", ex);
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Unauthorized();
		}

		[HttpPost("logout")]
		public async Task<IActionResult> LogOut()
		{
			try
			{
				var result = this.SignOut(new AuthenticationProperties { IsPersistent = true }, new string[] { IdentityConstants.ApplicationScheme });

				await this._signInManager.SignOutAsync();

				return Ok();
			}
			catch (Exception ex)
			{
				this._logger.LogError($"[{nameof(UserController)}][{(nameof(LogOut))}] Error occured.", ex);
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}
		}

		/// <summary>
		/// Get JWT from user claims
		/// </summary>
		/// <returns>jwt/message if the user is not authenticated</returns>
		[HttpGet]
		[AllowAnonymous]
		public string GetJWTToken()
		{
			var token = HttpContext.User.Claims.FirstOrDefault(c => c.Type == Default.AccessToken);
			return token != null ? token.Value : "UnAuthenticated User";
		}

		#endregion User Sign

		#region User Manager

		//[Authorize(Roles = "SuperAdmin,Admin,SubAdmin")]
		[HttpGet("all")]
		public async Task<IActionResult> GetUsersAsync(CancellationToken cancellationToken = default)
		{
			this._logger.LogInformation($"[{nameof(UserController)}][{(nameof(GetUsersAsync))}] Fetching Records.");

			try
			{
				var shopUsers = await this._userManager.Users.ToListAsync(cancellationToken);

				return Ok(this._mapper.Map<List<UserDto>>(shopUsers));
			}
			catch (Exception ex)
			{
				this._logger.LogError($"[{nameof(UserController)}][{(nameof(GetUsersAsync))}] Error occured.", ex);
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}
		}

		//[Authorize(Roles = "SuperAdmin,Admin,SubAdmin")]
		[HttpPost("add")]
		public async Task<IActionResult> AddUser(UserDto shopUser)
		{
			this._logger.LogError($"[{nameof(UserController)}][{(nameof(AddUser))}] Adding records: {JsonConvert.SerializeObject(shopUser)}.");

			if (ModelState.IsValid)
			{
				var user = this._mapper.Map<ShopUser>(shopUser);
				var existingUser = this._userManager.Users.FirstOrDefaultAsync(x => x.PhoneNumber == shopUser.PhoneNumber || x.Email == shopUser.Email, this.HttpContext.RequestAborted);

				if (existingUser != null)
				{
					return BadRequest(Message.Exists);
				}

				using (var transaction = await this._repository.Context.Database.BeginTransactionAsync(this.HttpContext.RequestAborted))
				{
					try
					{
						var result = await this._userManager.CreateAsync(user);

						if (result.Succeeded)
						{
							var roles = await this.GetRoles();

							if (roles.Count == 0)
							{
								result = await this._roleManager.CreateAsync(new IdentityRole
								{
									Name = DefaultRole.SuperAdmin,
								});

								result = await this._roleManager.CreateAsync(new IdentityRole
								{
									Name = DefaultRole.Seller,
								});

								result = await this._roleManager.CreateAsync(new IdentityRole
								{
									Name = DefaultRole.Buyer,
								});
							}

							if (result.Succeeded) //If there is no roles present in the db then seed the roles and if roles already exists then the user is successfully created.
							{
								result = await this._userManager.AddToRoleAsync(user, shopUser.Role);

								if (result.Succeeded)
								{
									await transaction.CommitAsync(this.HttpContext.RequestAborted);

									return Created("api/user/all", this._mapper.Map<UserDto>(shopUser));
								}
							}
						}

						await transaction.DisposeAsync();
						return Problem(this.GetError(result.Errors), null, (int?)ProblemStatusCode.Error);
					}
					catch (Exception ex)
					{
						this._logger.LogError($"[{nameof(UserController)}][{(nameof(AddUser))}] Error occured.", ex);
						await transaction.DisposeAsync();
						return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
					}
				}
			}

			return BadRequest(this.ModelState);
		}

		//[Authorize(Roles = "SuperAdmin,Admin,SubAdmin")]
		[HttpPut("update/{userId}")]
		public async Task<IActionResult> UpdateUser(string userId, UserDto userDto, CancellationToken cancellationToken = default)
		{
			this._logger.LogError($"[{nameof(UserController)}][{(nameof(UpdateUser))}] Updating user id: {userId} and updated data: {JsonConvert.SerializeObject(userDto)}.");

			if (string.IsNullOrEmpty(userId) || userDto == null || !this.ModelState.IsValid)
			{
				return BadRequest(this.ModelState);
			}

			ShopUser shopUser = await this._userManager.Users.FirstOrDefaultAsync(u => u.Id == userId, cancellationToken);

			if (shopUser != null)
			{
				IdentityResult result = null;
				using (var transaction = await this._repository.Context.Database.BeginTransactionAsync(this.HttpContext.RequestAborted))
				{
					try
					{
						if (!this.IsNotModified(userDto, shopUser))
						{
							shopUser = this._mapper.Map<ShopUser>(userDto);

							result = await this._userManager.UpdateAsync(shopUser);
						}

						var userRole = await this._userManager.GetRolesAsync(shopUser);

						if ((result == null || result.Succeeded || userRole == null) && !this.IsSameRole(userDto.Role, userRole))
						{
							result = await this._userManager.RemoveFromRoleAsync(shopUser, userRole[0]);

							if (result.Succeeded)
							{
								var addedRoleResult = await this._userManager.AddToRoleAsync(shopUser, userDto.Role);
							}
						}

						if (result == null || result.Succeeded)
						{
							await transaction.CommitAsync(this.HttpContext.RequestAborted);

							return Ok(this._mapper.Map<UserDto>(shopUser));
						}

						await transaction.DisposeAsync();
						return Problem(this.GetError(result.Errors), null, (int?)ProblemStatusCode.Error);
					}
					catch (Exception ex)
					{
						this._logger.LogError($"[{nameof(UserController)}][{(nameof(UpdateUser))}] Error occured.", ex);
						await transaction.DisposeAsync();
						return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
					}
				}
			}

			return NotFound();
		}

		//[Authorize(Roles = "SuperAdmin,Admin,SubAdmin")]
		[HttpDelete("{userId}")]
		public async Task<IActionResult> DeleteUser(string userId, CancellationToken cancellationToken = default)
		{
			this._logger.LogError($"[{nameof(UserController)}][{(nameof(AddUser))}] Deleting user id: {userId}.");

			if (string.IsNullOrWhiteSpace(userId))
			{
				return new StatusCodeResult((int)HttpStatusCode.BadRequest);
			}

			try
			{
				var user = await this._userManager.Users.FirstOrDefaultAsync(x => x.Id == userId, cancellationToken);

				if (user == null)
				{
					return NotFound();
				}

				var result = await this._userManager.DeleteAsync(user);

				if (result.Succeeded)
				{
					return Ok(this._mapper.Map<UserDto>(user));
				}

				return Problem(this.GetError(result.Errors), null, (int?)ProblemStatusCode.Error);
			}
			catch (Exception ex)
			{
				this._logger.LogError($"[{nameof(UserController)}][{(nameof(AddUser))}] Error occured.", ex);
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}
		}

		#endregion User Manager

		#region Helper Methods

		private async Task<ShopUser> GetUserAsync(string userNameUpper, CancellationToken cancellationToken = default)
		{
			int phoneNumber = 0;

			//using (HttpClient client = new HttpClient())
			//{
			//	client.DefaultRequestHeaders.Accept.Clear();
			//	client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			//	client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
			//	HttpResponseMessage response = await client.GetAsync(url);

			//	if (!response.IsSuccessStatusCode)
			//	{
			//		accessToken = authService.GenerateJWTAsync().Result;
			//	}
			//}

			if (userNameUpper.Contains("@"))
			{
				return await this._userManager.Users.FirstAsync(u => u.NormalizedEmail.Contains(userNameUpper), cancellationToken);
			}
			else if (int.TryParse(userNameUpper, out phoneNumber))
			{
				return await this._userManager.Users.FirstAsync(u => u.PhoneNumber.Contains(userNameUpper), cancellationToken);
			}

			return await this._userManager.Users.FirstAsync(u => u.NormalizedUserName == userNameUpper, cancellationToken);
		}

		private async Task SignInAsync(ShopUser shopUser)
		{
			try
			{
				string accesstoken = await _authService.GenerateJWTAsync(shopUser);

				IList<Claim> claims = await _userManager.GetClaimsAsync(shopUser);
				claims.Add(new Claim(Default.AccessToken, accesstoken));

				var claimsPrincipal = await _signInManager.CreateUserPrincipalAsync(shopUser);
				if (claims != null && claimsPrincipal?.Identity is ClaimsIdentity claimsIdentity)
					claimsIdentity.AddClaims(claims);

				await _signInManager.Context.SignInAsync(IdentityConstants.ApplicationScheme,
						claimsPrincipal,
						new AuthenticationProperties { IsPersistent = true });
			}
			catch (Exception ex)
			{
				this._logger.LogError($"[{nameof(UserController)}][{nameof(SignInAsync)} Error occured while storing signin claims.", ex);
			}
		}

		private string GetError(IEnumerable<IdentityError> errors)
		{
			return string.Join(',', errors.Select(x => x.Description));
		}

		private async Task<IList<string>> GetRoles()
		{
			return await this._roleManager.Roles.Select(x => x.Name).ToListAsync(this.HttpContext.RequestAborted);
		}

		private bool IsNotModified(UserDto userDto, ShopUser shopUser)
		{
			return userDto.FullName == shopUser.FullName && userDto.Email == shopUser.Email
				&& userDto.PhoneNumber == shopUser.PhoneNumber && userDto.IsDeleted == shopUser.IsDeleted
				&& userDto.Address == shopUser.Address;
		}

		private bool IsSameRole(string roleFromDto, IList<string> existingRoles)
		{
			string existingRole = string.Join(',', existingRoles);
			return roleFromDto == existingRole;
		}

		#endregion Helper Methods
	}
}
