﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.DependencyInjection.Models;
using Blazor.Shop.DependencyInjection.Models.Dtos;
using Blazor.Shop.DependencyInjection.Services;

using Microsoft.AspNetCore.Mvc;

namespace Blazor.Shop.Api.Controllers
{
	//[Authorize]
	[Route("api/{controller}")]
	[ApiController]
	public class ShopController : ControllerBase
	{
		#region Field Or Variables

		private readonly IShopService _shopService;

		#endregion Field Or Variables

		#region Constructors

		public ShopController(IShopService shopService)
		{
			this._shopService = shopService;
		}

		#endregion Constructors

		#region Products

		[HttpGet("product/{productId}")]
		public async Task<IActionResult> GetProductAsync(string productId, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrEmpty(productId))
			{
				return new StatusCodeResult((int)HttpStatusCode.BadRequest);
			}

			var product = await this._shopService.GetProductAsync(productId, cancellationToken);

			if (product == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			if (string.IsNullOrEmpty(product.Id))
			{
				return NotFound();
			}

			return Ok(product);
		}

		[HttpGet("product/all")]
		public async Task<IActionResult> GetProductsAsync(CancellationToken cancellationToken = default)
		{
			var products = await this._shopService.GetProductsAsync(cancellationToken);

			if (products == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			if (!products.Any())
			{
				return NotFound();
			}

			return Ok(products);
		}

		[HttpGet("product/report")]
		public async Task<IActionResult> GetProductReportAsync(CancellationToken cancellationToken = default)
		{
			//var products = await this.shopService.GetProductReportAsync(cancellationToken);

			//if (products == null)
			//{
			//	return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			//}

			//if (products.Count() == 0)
			//{
			return NotFound();
			//}

			//return Ok(products);
		}

		[HttpGet("product/details/{productId}")]
		public async Task<IActionResult> GetProductDetailsAsync(string productId, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrEmpty(productId))
			{
				return new StatusCodeResult((int)HttpStatusCode.BadRequest);
			}

			//var products = await this.shopService.GetProductDetailsAsync(productId, cancellationToken);

			//if (products == null)
			//{
			//	return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			//}

			//if (products.Count() == 0)
			//{
			return NotFound();
			//}

			//return Ok(products);
		}

		[HttpPost("product")]
		public async Task<IActionResult> AddProductAsync(ProductDto product, CancellationToken cancellationToken = default)
		{
			if (product == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.BadRequest);
			}

			var addedProduct = await this._shopService.AddProductAsync(product, cancellationToken);

			if (addedProduct == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Created(Url.ActionLink(), addedProduct);
		}

		[HttpPost("product/multiple")]
		public async Task<IActionResult> AddProductsAsync(IEnumerable<ProductDto> products, CancellationToken cancellationToken = default)
		{
			if (products == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.BadRequest);
			}

			var addedProducts = await this._shopService.AddProductsAsync(products, cancellationToken);

			if (addedProducts == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Created(Url.ActionLink(), addedProducts);
		}

		[HttpPut("product/{productId}")]
		public async Task<IActionResult> UpdateProductAsync(string productId, ProductDto product, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrEmpty(productId) || product == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.BadRequest);
			}

			var updatedProduct = await this._shopService.UpdateProductAsync(productId, product, cancellationToken);

			if (updatedProduct == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(updatedProduct);
		}

		[HttpDelete("product/{productId}")]
		public async Task<IActionResult> DeleteProductAsync(string productId, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrEmpty(productId))
			{
				return new StatusCodeResult((int)HttpStatusCode.BadRequest);
			}

			var deletedProduct = await this._shopService.DeleteProductAsync(productId, cancellationToken);

			if (deletedProduct == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(deletedProduct);
		}

		#endregion Products

		#region Sold Products

		[HttpGet("sold/all")]
		public async Task<IActionResult> GetSoldListAsync(CancellationToken cancellationToken = default)
		{
			var soldProducts = await this._shopService.GetSoldListAsync(cancellationToken);

			if (soldProducts == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			if (!soldProducts.Any())
			{
				return NotFound();
			}

			return Ok(soldProducts);
		}

		[HttpGet("sold/{soldId}")]
		public async Task<IActionResult> GetSoldByIdAsync(string soldId, CancellationToken cancellationToken = default)
		{
			var soldProducts = await this._shopService.GetSoldByIdAsync(soldId, cancellationToken);

			if (soldProducts == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			if (soldProducts == null || soldProducts.SoldProducts == null)
			{
				return NotFound();
			}

			return Ok(soldProducts);
		}

		[HttpGet("sold/user/{userId}")]
		public async Task<IActionResult> GetSoldsByUserIdAsync(string userId, CancellationToken cancellationToken = default)
		{
			var soldProducts = await this._shopService.GetSoldsByUserIdAsync(userId, cancellationToken);

			if (soldProducts == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			if (!soldProducts.Any())
			{
				return NotFound();
			}

			return Ok(soldProducts);
		}

		[HttpPost("sold")]
		public async Task<IActionResult> AddSoldProductAsync(Sold soldDto, CancellationToken cancellationToken = default)
		{
			if (soldDto == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.BadRequest);
			}

			var adddedSoldProduct = await this._shopService.AddSoldsPerUserAsync(soldDto, cancellationToken);

			if (adddedSoldProduct == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Created(Url.ActionLink(), adddedSoldProduct);
		}

		[HttpPut("sold/{soldId}")]
		public async Task<IActionResult> UpdateSoldProductAsync(string soldId, Sold soldDto, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrEmpty(soldId) || soldDto == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.BadRequest);
			}

			var updatedSoldProduct = await this._shopService.UpdateSoldProductAsync(soldId, soldDto, cancellationToken);

			if (updatedSoldProduct == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(updatedSoldProduct);
		}

		[HttpDelete("sold/{soldId}")]
		public async Task<IActionResult> DeleteSoldByIdAsync(string soldId, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrEmpty(soldId))
			{
				return new StatusCodeResult((int)HttpStatusCode.BadRequest);
			}

			var deletedSold = await this._shopService.DeleteSoldById(soldId, cancellationToken);

			if (deletedSold == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(deletedSold);
		}

		[HttpDelete("sold/product/{soldProductId}")]
		public async Task<IActionResult> DeleteSoldProductById(string soldProductId, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrEmpty(soldProductId))
			{
				return new StatusCodeResult((int)HttpStatusCode.BadRequest);
			}

			var deletedSoldProduct = await this._shopService.DeleteSoldProductById(soldProductId, cancellationToken);

			if (deletedSoldProduct == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(deletedSoldProduct);
		}

		#endregion Sold Products

		#region Product VATs

		[HttpGet("product/vat/all")]
		public async Task<IActionResult> GetVatsAsync(CancellationToken cancellationToken = default)
		{
			var vats = await this._shopService.GetVatsAsync(cancellationToken);

			if (vats == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			if (!vats.Any())
			{
				return NotFound();
			}

			return Ok(vats);
		}

		[HttpGet("product/vat/{invoiceNumber}")]
		public async Task<IActionResult> GetVatByIdAsync(long invoiceNumber, CancellationToken cancellationToken = default)
		{
			if (invoiceNumber <= 0)
			{
				return BadRequest();
			}

			var vat = await this._shopService.GetVatByInvoiceNumberAsync(invoiceNumber, cancellationToken);

			if (vat == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(vat);
		}

		[HttpPost("product/vat")]
		public async Task<IActionResult> AddVatAsync(Vat vat, CancellationToken cancellationToken = default)
		{
			if (vat == null || vat.ProductVats == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.BadRequest);
			}

			var addedVat = await this._shopService.AddVatAsync(vat, cancellationToken);

			if (addedVat == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Created(Url.ActionLink(), addedVat);
		}

		#endregion Product VATs

		#region User Product Report

		[HttpGet("user/product/report")]
		public async Task<IActionResult> GetUserProductsReportAsync(string phoneNumber, CancellationToken cancellationToken = default)
		{
			var userProductsReport = await this._shopService.GetUserProductsReportAsync(phoneNumber, cancellationToken);

			if (userProductsReport == null || userProductsReport.UserProducts == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			if (userProductsReport.UserProducts.Count() == 0)
			{
				return NotFound();
			}

			return Ok(userProductsReport);
		}

		[HttpGet("user/product/purchase")]
		public async Task<IActionResult> GetUserPurchaseReportAsync(CancellationToken cancellationToken = default)
		{
			var purchasedProducts = await this._shopService.GetUserPurchaseReportAsync(cancellationToken);

			if (purchasedProducts == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			if (purchasedProducts.Count() == 0)
			{
				return NotFound();
			}

			return Ok(purchasedProducts);
		}

		#endregion User Product Report

		#region Symbolic Price

		[HttpGet("price/{symbolicPrice}")]
		public async Task<IActionResult> GetPriceBySymbolicPriceAsync(string symbolicPrice, CancellationToken cancellationToken = default)
		{
			decimal actualPrice = await this._shopService.GetPriceAsync(symbolicPrice, cancellationToken);

			if (actualPrice == -1)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(symbolicPrice);
		}

		[HttpGet("symbolic-price/{alphabet}")]
		public async Task<IActionResult> GetSymbolicPriceAsync(char alphabet, CancellationToken cancellationToken = default)
		{
			var symbolicPrice = await this._shopService.GetSymbolicPriceAsync(alphabet, cancellationToken);

			if (symbolicPrice == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			if (symbolicPrice.Value <= 0)
			{
				return NotFound();
			}

			return Ok(symbolicPrice);
		}

		[HttpGet("symbolic-price/all")]
		public async Task<IActionResult> GetSymbolicPricesAsync(CancellationToken cancellationToken = default)
		{
			var symbolicPrices = await this._shopService.GetSymbolicPricesAsync(cancellationToken);

			if (symbolicPrices == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			if (symbolicPrices.Count() == 0)
			{
				return NotFound();
			}

			return Ok(symbolicPrices);
		}

		[HttpPost("symbolic-price")]
		public async Task<IActionResult> AddSymbolicPricesAsync(SymbolicPrice symbolicPrice, CancellationToken cancellationToken = default)
		{
			if (symbolicPrice == null)
			{
				return BadRequest();
			}

			var addedSymbolicPrice = await this._shopService.AddSymbolicPriceAsync(symbolicPrice, cancellationToken);

			if (addedSymbolicPrice == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Created(Url.ActionLink(), addedSymbolicPrice);
		}

		[HttpPut("symbolic-price/{id}")]
		public async Task<IActionResult> UpdateSymbolicPricesAsync(string id, SymbolicPrice symbolicPrice, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrEmpty(id) || symbolicPrice == null)
			{
				return BadRequest();
			}

			var updatedSymbolicPrice = await this._shopService.UpdateSymbolicPriceAsync(id, symbolicPrice, cancellationToken);

			if (updatedSymbolicPrice == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(updatedSymbolicPrice);
		}

		[HttpDelete("symbolic-price/{id}")]
		public async Task<IActionResult> DeleteSymbolicPricesAsync(string id, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrEmpty(id))
			{
				return BadRequest();
			}

			var deletedSymbolicPrice = await this._shopService.DeleteSymbolicPriceAsync(id, cancellationToken);

			if (deletedSymbolicPrice == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(deletedSymbolicPrice);
		}

		#endregion Symbolic Price

		#region Company

		[HttpGet("company/{taxTypeNumber}")]
		public async Task<IActionResult> GetCompanyByTaxTypeNumber(string taxTypeNumber, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(taxTypeNumber))
			{
				return BadRequest();
			}

			var company = await this._shopService.GetCompanyByTaxTypeNumber(taxTypeNumber, cancellationToken);

			if(company == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(company);
		}

		[HttpGet("company/{companyId:Guid}")]
		public async Task<IActionResult> GetCompanyById(Guid companyId, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(companyId.ToString()) || Guid.Empty == companyId)
			{
				return BadRequest();
			}

			var company = await this._shopService.GetCompanyById(companyId.ToString(), cancellationToken);

			if (company == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(company);
		}

		[HttpGet("company/all")]
		public async Task<IActionResult> GetCompanies(CancellationToken cancellationToken = default)
		{
			var companies = await this._shopService.GetCompanies(cancellationToken);

			if (companies == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(companies);
		}

		[HttpPost("company")]
		public async Task<IActionResult> AddCompany(Company company, CancellationToken cancellationToken = default)
		{
			if (company == null)
			{
				return BadRequest();
			}

			var companyAdded = await this._shopService.AddCompany(company, cancellationToken);

			if (companyAdded == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Created($"company/{companyAdded.Id}", companyAdded);
		}

		[HttpPut("company/{companyId:Guid}")]
		public async Task<IActionResult> UpdateCompany(Guid companyId, Company company, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(companyId.ToString()) || companyId == Guid.Empty || company == null)
			{
				return BadRequest();
			}

			var companyUpdated = await this._shopService.UpdateCompany(companyId.ToString(), company, cancellationToken);

			if (companyUpdated == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(companyUpdated);
		}

		[HttpDelete("company/{companyId:Guid}")]
		public async Task<IActionResult> DeleteCompanyById(string companyId, CancellationToken cancellationToken = default)
		{
			if (!string.IsNullOrWhiteSpace(companyId) || Guid.Parse(companyId) == Guid.Empty)
			{
				return BadRequest();
			}

			var companyDeleted = await this._shopService.DeleteCompanyById(companyId, cancellationToken);

			if (companyDeleted == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(companyDeleted);
		}

		#endregion Company

		#region Company VAT

		[HttpGet("company/vat/{billNumber}")]
		public async Task<IActionResult> GetCompanyVatByBillNumber(string billNumber, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(billNumber))
			{
				return BadRequest();
			}

			var CompanyVat = await this._shopService.GetCompanyVatByBillNumber(billNumber, cancellationToken);

			if (CompanyVat == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(CompanyVat);
		}

		[HttpGet("company/vat/{companyVatId:Guid}")]
		public async Task<IActionResult> GetCompanyVatById(string companyVatId, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(companyVatId) || Guid.Parse(companyVatId) == Guid.Empty)
			{
				return BadRequest();
			}

			var CompanyVat = await this._shopService.GetCompanyVatById(companyVatId, cancellationToken);

			if (CompanyVat == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(CompanyVat);
		}

		[HttpGet("company/vat/all")]
		public async Task<IActionResult> GetCompanVats(CancellationToken cancellationToken = default)
		{
			List<CompanyVat> companyVats = await this._shopService.GetCompanyVats(cancellationToken);

			if (companyVats == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(companyVats);
		}

		[HttpPost("company/vat")]
		public async Task<IActionResult> AddCompanyVat(CompanyVat companyVat, CancellationToken cancellationToken = default)
		{
			if (companyVat == null)
			{
				return BadRequest();
			}

			var CompanyVatAdded = await this._shopService.AddCompanyVat(companyVat, cancellationToken);

			if (CompanyVatAdded == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(CompanyVatAdded);
		}

		[HttpPut("company/vat/{companyVatId:Guid}")]
		public async Task<IActionResult> UpdateCompanyVat(string companyVatId, CompanyVat companyVat, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(companyVatId) || Guid.Parse(companyVatId) == Guid.Empty || companyVat == null)
			{
				return BadRequest();
			}

			var CompanyVatUpdated = await this._shopService.UpdateCompanyVat(companyVatId, companyVat, cancellationToken);

			if (CompanyVatUpdated == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(CompanyVatUpdated);
		}

		[HttpDelete("company/vat/{companyVatId:Guid}")]
		public async Task<IActionResult> DeleteCompanyVatById(string companyVatId, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(companyVatId) || Guid.Parse(companyVatId) == Guid.Empty)
			{
				return BadRequest();
			}

			var CompanyVatDeleted = await this._shopService.DeleteCompanyVatById(companyVatId, cancellationToken);

			if (CompanyVatDeleted == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(CompanyVatDeleted);
		}

		#endregion Company VAT

		#region Sold Logs

		[HttpGet("sold/logs/{soldId}")]
		public async Task<IActionResult> GetLogsBySoldId(string soldId, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(soldId))
			{
				return BadRequest();
			}

			var logs = await this._shopService.GetLogsBySoldId(soldId, cancellationToken);

			if(logs == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(logs);
		}
		
		[HttpDelete("sold/logs/{soldId}")]
		public async Task<IActionResult> DeleteSoldLogById(string soldId, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(soldId))
			{
				return BadRequest();
			}

			var logs = await this._shopService.DeleteSoldLogById(soldId, cancellationToken);

			if(logs == null)
			{
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
			}

			return Ok(logs);
		}

		#endregion Sold Logs

		#region Helper Methods

		private IActionResult NotAvailableResponse(IEnumerable<SoldProduct> notSoldProduct)
		{
			return new JsonResult(new
			{
				Message = $"The product(s): {(string.Join(',', notSoldProduct.Select(x => x.ProductId)))} cannot be sold since it is not available.",
			});
		}

		private IActionResult NotAvailableResponse(IEnumerable<ProductVat> notAddedProductsVats)
		{
			return new JsonResult(new
			{
				Message = $"The product(s): {(string.Join(',', notAddedProductsVats.Select(x => x.Product.Code)))} Vat(s) cannot be sold. Please try again.",
			});
		}

		#endregion Helper Methods
	}
}
