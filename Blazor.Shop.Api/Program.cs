using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using Blazor.Shop.DependencyInjection.Common.Constants;
using Blazor.Shop.DependencyInjection.DemoData;
using Blazor.Shop.DependencyInjection.Models;
using Blazor.Shop.DependencyInjection.Repositories;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using NLog.Web;

namespace Blazor.Shop.Api
{
	public class Program
	{
		public static async Task Main(string[] args)
		{
			var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

			try
			{
				logger.Debug("init main");
				IHost host = CreateHostBuilder(args).Build();
				await SeedData(host);
				host.Run();
			}
			catch (Exception exception)
			{
				//NLog: catch setup errors
				logger.Error(exception, "Stopped program because of exception");
				throw;
			}
			finally
			{
				// Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
				NLog.LogManager.Shutdown();
			}
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
				Host.CreateDefaultBuilder(args)
						.ConfigureWebHostDefaults(webBuilder =>
						{
							webBuilder.UseStartup<Startup>();
						})
						.ConfigureLogging(logging =>
						{
							logging.ClearProviders();
							logging.SetMinimumLevel(LogLevel.Trace);
						})
						.UseNLog();  // NLog: Setup NLog for Dependency injection;

		public static async Task SeedData(IHost host)
		{
			//using (var scope = host.Services.CreateScope())
			//{
			//	IServiceProvider services = scope.ServiceProvider;
			//	IGenericRepository repository = services.GetRequiredService<IGenericRepository>();
			//	UserManager<ShopUser> userManager = services.GetRequiredService<UserManager<ShopUser>>();
			//	RoleManager<IdentityRole> roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();

			//	ShopUser user = await userManager.FindByIdAsync(DefaultUser.SubAdminId);
			//	int totalRoles = user != null ? userManager.GetRolesAsync(user).Result.Count() : 0;
			//	int productCount = repository.Table<Product>().Count();
			//	int soldProductCount = repository.Table<Sold>().Count();
			//	int symbolicPriceCount = repository.Table<SymbolicPrice>().Count();

			//	if (user == null)
			//	{
			//		foreach (ShopUser shopUser in DefaultDataProvider.Users)
			//		{
			//			await userManager.CreateAsync(shopUser);
			//		}
			//	}

			//	if (totalRoles == 0)
			//	{
			//		//Remove this methods in the future.
			//		foreach (IdentityRole role in DefaultDataProvider.Roles)
			//		{
			//			await roleManager.CreateAsync(role);
			//		}

			//		//for (int i = 0; i < DefaultDataProvider.Roles.Count; i++)
			//		//{
			//		//	await userManager.AddClaimAsync(DefaultDataProvider.Users[0], new Claim(ClaimTypes.Role, DefaultDataProvider.Roles[i].Name));
			//		//	await userManager.AddToRoleAsync(DefaultDataProvider.Users[0], DefaultDataProvider.Roles[i].Name);
			//		//}
			//	}

			//	if (productCount == 0)
			//	{
			//		await repository.AddMultipleAsync<Product>(DefaultDataProvider.Products);
			//	}

			//	if (soldProductCount == 0)
			//	{
			//		//await repository.AddMultiple<Sold>(DefaultDataProvider.SoldProducts);
			//	}

			//	if (symbolicPriceCount == 0)
			//	{
			//		await repository.AddMultipleAsync<SymbolicPrice>(DefaultDataProvider.SymbolicPrices);
			//	}
			//}
		}
	}
}
