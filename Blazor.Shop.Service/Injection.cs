﻿using System.Threading.Tasks;

using Blazor.Shop.DependencyInjection;
using Blazor.Shop.DependencyInjection.Services;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Blazor.Shop.Service.Services
{
	public class Injection : IInjection
	{
		public Task Inject(IServiceCollection services, IConfiguration configuration)
		{
			services.AddScoped<IShopService, ShopService>();
			services.AddScoped<IAuthService, AuthService>();

			return Task.CompletedTask;
		}
	}
}
