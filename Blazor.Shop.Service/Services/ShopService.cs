﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using Blazor.Shop.DependencyInjection.Models;
using Blazor.Shop.DependencyInjection.Models.Dtos;
using Blazor.Shop.DependencyInjection.Repositories;
using Blazor.Shop.DependencyInjection.Services;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

namespace Blazor.Shop.Service.Services
{
	public class ShopService : IShopService
	{
		#region Field Or Variables
		private bool disposed;

		private IGenericRepository _repository;
		private readonly ILogger<ShopService> _logger;
		private IMapper _mapper;

		#endregion Field Or Variables

		#region Constructors

		public ShopService(IGenericRepository repository, ILogger<ShopService> logger, IMapper mapper)
		{
			this._repository = repository;
			this._logger = logger;
			this._mapper = mapper;
		}

		#endregion Constructors

		#region Products

		public async Task<ProductDto> GetProductAsync(string id, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetProductAsync] Getting product id: {id}.");

			Product product = await this._repository.GetAsync<Product>(id, cancellationToken);
			ProductDto productDto = this._mapper.Map<ProductDto>(product);
			return productDto;
		}

		public async Task<IEnumerable<ProductDto>> GetProductsAsync(CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetProductsAsync] Getting products.");

			IEnumerable<Product> products = await this._repository.GetsAsync<Product>(cancellationToken);
			IEnumerable<ProductDto> productDtos = this._mapper.Map<IEnumerable<ProductDto>>(products);

			return productDtos;
		}

		public async Task<ProductDto> AddProductAsync(ProductDto productDto, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][AddProductAsync] Adding product: {JsonConvert.SerializeObject(productDto)}.");

			Product product = this._mapper.Map<Product>(productDto);

			Product addedProduct = await this._repository.AddAsync(product, cancellationToken);
			productDto = this._mapper.Map<ProductDto>(addedProduct);
			return productDto;
		}

		public async Task<IEnumerable<ProductDto>> AddProductsAsync(IEnumerable<ProductDto> productDtos, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][AddProductsAsync] Adding products: {JsonConvert.SerializeObject(productDtos)}.");

			List<Product> products = this._mapper.Map<List<Product>>(productDtos);

			IEnumerable<Product> addedProducts = await this._repository.AddMultipleAsync(products, cancellationToken);

			productDtos = this._mapper.Map<List<ProductDto>>(addedProducts);
			return productDtos;
		}

		public async Task<ProductDto> UpdateProductAsync(string id, ProductDto productDto, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][UpdateProductAsync] Updating product id: {id} with data: {JsonConvert.SerializeObject(productDto)}.");

			Product product = this._mapper.Map<Product>(productDto);

			Product updatedProduct = await this._repository.UpdateAsync(id, product, cancellationToken);

			productDto = this._mapper.Map<ProductDto>(updatedProduct);
			return productDto;
		}

		public async Task<ProductDto> DeleteProductAsync(string id, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][DeleteProductAsync] Deleting product id : {id}.");

			Product deletedProduct = await this._repository.DeleteAsync<Product>(id, cancellationToken);
			ProductDto productDto = this._mapper.Map<ProductDto>(deletedProduct);

			return productDto;
		}

		#endregion Products

		#region Sold Products

		public async Task<Sold> GetSoldByIdAsync(string soldId, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetSoldProductsByIdAsync] Getting list of sold products by user id: {soldId}.");

			return await this.Execute(async () =>
			{
				Sold sold = await this._repository
											.Table<Sold>()
											.Include(x => x.Buyer)
											.Include(x => x.Seller)
											.Include(x => x.SoldLogs)
											.Include(x => x.SoldProducts)
											.ThenInclude(x => x.Product)
											.FirstOrDefaultAsync(x => x.Id == soldId, cancellationToken);

				return sold;
			},
				$"[${nameof(ShopService)}][${nameof(GetSoldByIdAsync)}] Error occured while getting sold products by id: {soldId}.");
		}

		public async Task<IEnumerable<Sold>> GetSoldsByUserIdAsync(string userId, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetSoldProductsByUserIdAsync] Getting list of sold products by user id: {userId}.");

			return await this.Execute(async () =>
			{
				IEnumerable<Sold> soldProducts = await this._repository
											.Table<Sold>()
											.Include(x => x.Buyer)
											.Include(x => x.Seller)
											.Include(x => x.SoldProducts)
											.ThenInclude(x => x.Product)
											.Where(x => x.BuyerId == userId)
											.ToListAsync(cancellationToken);


				return soldProducts;
			},
				$"[${nameof(ShopService)}][${nameof(GetSoldsByUserIdAsync)}] Error occured while getting sold products by user id: {userId}.");
		}

		public async Task<IEnumerable<Sold>> GetSoldListAsync(CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetSoldListAsync] Getting sold products.");

			return await this.Execute(async () =>
			{
				var soldProducts = this._repository
									.Table<Sold>()
									.Include(x => x.Buyer)
									.Include(x => x.Seller)
									.Include(x => x.SoldProducts)
									.OrderBy(x => x.BuyerId);

				if (!this.IsSqlite())
				{
					return await soldProducts.ThenByDescending(x => x.CreationDate)
									.ToListAsync(cancellationToken);
				}

				return await soldProducts.ToListAsync(cancellationToken);
			},
				$"[${nameof(ShopService)}][${nameof(GetSoldListAsync)}] Error occured while getting sold products.");
		}

		public async Task<Sold> AddSoldsPerUserAsync(Sold soldDto, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(soldDto.BuyerId))
			{
				throw new ArgumentNullException("BuyerId");
			}

			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][AddSoldProductsPerUserAsync] Adding sold products per user: {JsonConvert.SerializeObject(soldDto)}.");

			var existinProducts = (await this._repository.GetsAsync<Product>(cancellationToken, w => soldDto.SoldProducts.Select(x => x.Product.Code).Contains(w.Code))).ToList();

			for (int i = 0; i < soldDto.SoldProducts.Count(); i++)
			{
				for (int j = 0; j < existinProducts.Count(); j++)
				{
					if (existinProducts[j].Code == soldDto.SoldProducts[i].Product?.Code)
					{
						existinProducts[j].AvailableQuantity = soldDto.SoldProducts[i].Product.AvailableQuantity;
						//Clearing to remove loop referential
						soldDto.SoldProducts[i].ProductId = existinProducts[j].Id;
						soldDto.SoldProducts[i].Product = null;
					}
				}
			}

			soldDto.Buyer = null;
			soldDto.Seller = null;

			using (var transaction = await this._repository.Context.Database.BeginTransactionAsync())
			{
				try
				{
					var result = await this._repository.AddAsync(soldDto, cancellationToken);

					if (result != null)
					{
						for (int j = 0; j < existinProducts.Count(); j++)
						{
							// detach
							this._repository.Context.Entry(soldDto.SoldProducts[j].Product).State = EntityState.Detached;

							// set Modified flag in your entry
							this._repository.Context.Entry(existinProducts[j]).State = EntityState.Modified;
							existinProducts[j].SoldProducts?.Clear();
							existinProducts[j].SoldProducts = null;
						}

						var updateResult = await this._repository.UpdateMultipleAsync<Product>(existinProducts, cancellationToken);

						if (updateResult != null)
						{
							var soldLog = new SoldLog()
							{
								SoldId = result.Id,
								TotalAmount = soldDto.TotalAmount,
								TotalDiscountAmount = soldDto.TotalDiscountAmount,
								TotalPaidAmount = soldDto.TotalPaidAmount,
								TotalPendingAmount = soldDto.TotalPendingAmount
							};

							var soldLogResult = await this._repository.AddAsync(soldLog, cancellationToken);

							if (soldLogResult != null)
							{
								await transaction.CommitAsync();
								return result;
							}
						}
					}

					await transaction.DisposeAsync();
				}
				catch (Exception ex)
				{
					this._logger.LogError($"[ShopService][AddSoldProductsPerUserAsync] Error occured.", ex);
					await transaction.DisposeAsync();
				}
			}

			return null;
		}

		public async Task<Sold> UpdateSoldProductAsync(string soldId, Sold soldDto, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][UpdateSoldProductAsync] Updating sold products: {JsonConvert.SerializeObject(soldDto)} by sold id: {soldId}.");

			var toBeUpdtedProductList = new List<Product>();

			for (int i = 0; i < soldDto.SoldProducts.Count(); i++)
			{
				toBeUpdtedProductList.Add(new Product
				{
					Id = soldDto.SoldProducts[i].ProductId,
					AvailableQuantity = soldDto.SoldProducts[i].Product.AvailableQuantity
				});
			}

			using (var transaction = await this._repository.Context.Database.BeginTransactionAsync())
			{
				try
				{
					soldDto.SoldProducts.Clear(); //Remove these to avoid duplcates entry into the database.
					soldDto.SoldProducts = null;

					Sold updatedSold = await this._repository.UpdateAsync(soldId, soldDto);

					if (updatedSold != null)
					{
						if (soldDto.SoldProducts != null)
						{
							for (int j = 0; j < soldDto.SoldProducts.Count(); j++)
							{
								// detach
								this._repository.Context.Entry(soldDto.SoldProducts[j].Product).State = EntityState.Detached;

								// set Modified flag in your entry
								this._repository.Context.Entry(toBeUpdtedProductList[j]).State = EntityState.Modified;
								toBeUpdtedProductList[j].SoldProducts?.Clear();
								toBeUpdtedProductList[j].SoldProducts = null;
							}
						}

						var result = await this._repository.UpdateMultipleAsync<Product>(toBeUpdtedProductList, cancellationToken);

						if (result != null)
						{
							var soldLog = new SoldLog()
							{
								SoldId = updatedSold.Id,
								TotalAmount = soldDto.TotalAmount,
								TotalDiscountAmount = soldDto.TotalDiscountAmount,
								TotalPaidAmount = soldDto.TotalPaidAmount,
								TotalPendingAmount = soldDto.TotalPendingAmount
							};

							var soldLogResult = await this._repository.AddAsync(soldLog, cancellationToken);

							if (soldLogResult != null)
							{
								await transaction.CommitAsync();
								return updatedSold;
							}
						}
					}

					await transaction.DisposeAsync();
				}
				catch (Exception ex)
				{
					this._logger.LogError($"[ShopService][UpdateSoldProductAsync] Error occured.", ex);
					await transaction.DisposeAsync();
				}
			}

			return null;
		}

		public async Task<Sold> DeleteSoldById(string soldId, CancellationToken cancellationToken = default)
		{
			this._logger.LogInformation($"[ShopService][DeleteSoldById] Deleting sold id: {soldId}.");

			return await this._repository.DeleteAsync<Sold>(soldId, cancellationToken);
		}

		public async Task<SoldProduct> DeleteSoldProductById(string soldProductId, CancellationToken cancellationToken = default)
		{
			this._logger.LogInformation($"[ShopService][DeleteSoldProductById] sold product id: {soldProductId}.");

			return await this._repository.DeleteAsync<SoldProduct>(soldProductId, cancellationToken);
		}

		#endregion Sold Products

		#region Product VATs

		public async Task<Vat> GetVatByInvoiceNumberAsync(long invoiceNumber, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetVatByIdAsync] Getting vat by id: {invoiceNumber}.");

			return await this.Execute(async () =>
			{
				var vat = this._repository.Table<Vat>()
									.Include(x => x.ProductVats)
									.ThenInclude(x => x.Product);

				if (!this.IsSqlite())
				{
					return await vat.OrderBy(x => x.CreationDate)
									.FirstOrDefaultAsync(x => x.InvoiceNumber == invoiceNumber, cancellationToken);
				}

				return await vat.FirstOrDefaultAsync(x => x.InvoiceNumber == invoiceNumber, cancellationToken);
			},
				$"[${nameof(ShopService)}][${nameof(GetVatByInvoiceNumberAsync)}] Error occured while getting vat by id: {invoiceNumber}.");
		}

		public async Task<IEnumerable<Vat>> GetVatsAsync(CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetVatsAsync] Getting vats.");

			return await this.Execute(async () =>
			{
				var vats = this._repository.Table<Vat>()
									.Include(x => x.ProductVats);

				if (!this.IsSqlite())
				{
					return await vats.OrderByDescending(x => x.CreationDate)
									.ToListAsync(cancellationToken);
				}

				return await vats.ToListAsync(cancellationToken);
			},
				$"[${nameof(ShopService)}][${nameof(GetVatsAsync)}] Error occured while getting vats.");
		}

		public async Task<Vat> AddVatAsync(Vat vat, CancellationToken cancellationToken = default)
		{
			this._logger.LogInformation($"[ShopService][AddVatAsync] Adding vat: {JsonConvert.SerializeObject(vat)}.");

			var existinProducts = await this._repository.GetsAsync<Product>(cancellationToken, w => vat.ProductVats.Select(x => x.Product.Code).Contains(w.Code));

			for (int i = 0; i < vat.ProductVats.Count(); i++)
			{
				foreach (var product in existinProducts)
				{
					if (product.Code == vat.ProductVats[i].Product?.Code)
					{
						vat.ProductVats[i].Product = null;
						vat.ProductVats[i].ProductId = product.Id;
					}
				}
			}

			Vat addedVat = await this._repository.AddAsync<Vat>(vat, cancellationToken);
			return addedVat;
		}

		#endregion Product VATs

		#region User Product Report

		public async Task<UserProductReportDto> GetUserProductsReportAsync(string phoneNumber, CancellationToken cancellationToken = default)
		{
			throw new NotImplementedException();

			//try
			//{
			//	var userProducts = await this.repository.Table<Sold>()
			//							.Where(s => s.BuyerPhones.Contains(phoneNumber))
			//							.Include(x => x.Product)
			//							.Select(x => new UserProductDto
			//							{
			//								ProductName = x.Product.Name,
			//								ProductCode = x.Product.Code,
			//								Quantity = x.Quantity,
			//								TotalAmount = x.TotalAmount,
			//								PaidAmount = x.PaidAmount,
			//								PendingAmount = x.PendingAmount,
			//								DiscountAmount = x.DiscountAmount,
			//								PurchasedDate = x.CreationDate
			//							})
			//							.OrderBy(x => x.PurchasedDate)
			//							.ToListAsync(cancellationToken);

			//	var user = this.repository.Table<ShopUser>().FirstOrDefault(x => x.PhoneNumber.Contains(phoneNumber));

			//	return new UserProductReportDto
			//	{
			//		FullName = user.FullName,
			//		Phones = user.PhoneNumber,
			//		Emails = user.Email,
			//		Address = user.Address,
			//		UserProducts = userProducts
			//	};
			//}
			//catch (OperationCanceledException ex)
			//{
			//	this.logger.LogError($"[ShopService][GetUserProductsReportAsync] Operation cancelled while getting user product purchased report.", ex);

			//	return new UserProductReportDto
			//	{
			//		IsCancelled = true,
			//		Phones = phoneNumber
			//	};
			//}
			//catch (Exception ex)
			//{
			//	this.logger.LogError($"[ShopService][GetUserProductsReportAsync] Error occured while getting user product purchased report.", ex);
			//	return null;
			//}
		}

		public async Task<IEnumerable<UserPurchaseDto>> GetUserPurchaseReportAsync(CancellationToken cancellationToken = default)
		{
			throw new NotImplementedException();
			//try
			//{
			//	var purchases = await this.repository.Table<Product>()
			//						.Include(x => x.SoldProducts)
			//						.Select(x => new UserPurchaseDto
			//						{
			//							FullName = x.SoldProducts.FirstOrDefault(y => y.ProductId == x.Id).BuyerFullName,
			//							Emails = x.SoldProducts.FirstOrDefault(y => y.ProductId == x.Id).BuyerEmails,
			//							Phones = x.SoldProducts.FirstOrDefault(y => y.ProductId == x.Id).BuyerPhones,
			//							Address = x.SoldProducts.FirstOrDefault(y => y.ProductId == x.Id).BuyerAddress,
			//							Quantity = x.SoldProducts.Sum(y => y.Quantity),
			//							TotalAmount = x.SoldProducts.Sum(y => y.TotalAmount),
			//							TotalAmountPaid = x.SoldProducts.Sum(y => y.PaidAmount),
			//							TotalPendingAmount = x.SoldProducts.Sum(y => y.PendingAmount),
			//							TotalDiscountAmount = x.SoldProducts.Sum(y => y.DiscountAmount),
			//						})
			//						.ToListAsync(cancellationToken);

			//	return purchases;
			//}
			//catch (OperationCanceledException ex)
			//{
			//	this.logger.LogError($"[ShopService][GetUserPurchaseReportAsync] Operation cancelled while getting user purchased report.", ex);

			//	return new List<UserPurchaseDto>{
			//			new UserPurchaseDto
			//			{
			//				OperationCancelled = true
			//			}
			//		};
			//}
			//catch (Exception ex)
			//{
			//	this.logger.LogError($"[ShopService][GetUserPurchaseReportAsync] Error occured while getting user purchased report.", ex);
			//	return null;
			//}
		}

		#endregion User Product Report

		#region Symbolic Price

		public async Task<SymbolicPrice> GetSymbolicPriceAsync(char alphabet, CancellationToken cancellationToken = default)
		{
			this._logger.LogInformation($"[ShopService][GetSymbolicPriceAsync] Getting symbolic price by alphabet: {alphabet}.");

			SymbolicPrice symbolicPrice = await this._repository.GetAsync<SymbolicPrice>(x => x.Alphabet == alphabet, cancellationToken);
			return symbolicPrice;
		}

		public async Task<decimal> GetPriceAsync(string symbolicPrice, CancellationToken cancellationToken = default)
		{
			this._logger.LogInformation($"[ShopService][GetPriceAsync] Getting price by symbolic price: {symbolicPrice}.");

			IEnumerable<SymbolicPrice> symbolicPrices = await this.GetSymbolicPricesAsync(cancellationToken);
			decimal value = -1;

			if (!symbolicPrices.First().IsCancelled)
			{
				value = 0;
				string result = string.Empty;
				bool notNumber = false;

				if (!decimal.TryParse(symbolicPrice, out value))
				{
					foreach (char c in symbolicPrice)
					{
						var price = symbolicPrices.FirstOrDefault(p => p.Alphabet == c);
						result += price == null ? c + string.Empty : price.Value + string.Empty;
					}

					notNumber = true;
				}

				if (notNumber)
				{
					decimal.TryParse(result, out value);
				}
			}

			return value;
		}

		public async Task<IEnumerable<SymbolicPrice>> GetSymbolicPricesAsync(CancellationToken cancellationToken = default)
		{
			this._logger.LogInformation($"[ShopService][GetSymbolicPricesAsync] Getting symbolic prices.");

			IEnumerable<SymbolicPrice> symbolicPrices = await this._repository.Table<SymbolicPrice>()
														.OrderBy(x => x.Alphabet)
														.ToListAsync(cancellationToken);
			return symbolicPrices;
		}

		public async Task<SymbolicPrice> AddSymbolicPriceAsync(SymbolicPrice symbolicPrice, CancellationToken cancellationToken = default)
		{
			this._logger.LogInformation($"[ShopService][AddSymbolicPriceAsync] Adding symbolic price: {JsonConvert.SerializeObject(symbolicPrice)}.");

			SymbolicPrice addedSymbolicPrice = await this._repository.AddAsync(symbolicPrice, cancellationToken);
			return addedSymbolicPrice;
		}

		public async Task<SymbolicPrice> UpdateSymbolicPriceAsync(string id, SymbolicPrice symbolicPrice, CancellationToken cancellationToken = default)
		{
			this._logger.LogInformation($"[ShopService][UpdateSymbolicPriceAsync] Updating symbolic price: {JsonConvert.SerializeObject(symbolicPrice)} by id: {id}.");

			SymbolicPrice updatedSymbolicPrice = await this._repository.UpdateAsync(id, symbolicPrice, cancellationToken);
			return updatedSymbolicPrice;
		}

		public async Task<SymbolicPrice> DeleteSymbolicPriceAsync(string id, CancellationToken cancellationToken = default)
		{
			this._logger.LogInformation($"[ShopService][DeleteSymbolicPriceAsync] Deleting symbolic price by id: {id}.");

			SymbolicPrice deletedSymbolicPrice = await this._repository.DeleteAsync<SymbolicPrice>(id, cancellationToken);
			return deletedSymbolicPrice;
		}

		#endregion Symbolic Price

		#region Company

		public async Task<Company> GetCompanyById(string companyId, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetCompanyById] Getting company by company id: {companyId}.");
			return await this._repository.GetAsync<Company>(companyId, cancellationToken);
		}

		public async Task<Company> GetCompanyByTaxTypeNumber(string taxTypeNumber, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetCompanyByTaxTypeNumber] Getting company by company tax type number (PAN/VAT): {taxTypeNumber}.");
			return await this._repository.GetAsync<Company>(taxTypeNumber, cancellationToken);
		}

		public async Task<List<Company>> GetCompanies(CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetCompanies] Getting companies.");
			return (await this._repository.GetsAsync<Company>(cancellationToken)).ToList();
		}

		public async Task<Company> AddCompany(Company company, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][AddCompany] Adding company: {JsonConvert.SerializeObject(company)}.");
			return await this._repository.AddAsync(company, cancellationToken);
		}

		public async Task<Company> UpdateCompany(string id, Company company, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][UpdateCompany] Updating company: {JsonConvert.SerializeObject(company)}.");
			return await this._repository.UpdateAsync(id, company, cancellationToken);
		}

		public async Task<Company> DeleteCompanyById(string id, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][DeleteCompanyById] Deleting company by id: {id}.");
			return await this._repository.DeleteAsync<Company>(id, cancellationToken);
		}

		#endregion Company

		#region Company VAT

		public async Task<CompanyVat> GetCompanyVatByBillNumber(string billNumber, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetCompanyVatByBillNumber] Getting CompanyVat by CompanyVat bill number: {billNumber}.");
			return await this.Execute(() => this._repository.Table<CompanyVat>().Include(x => x.Company).FirstOrDefaultAsync(x => x.BillNumber == billNumber, cancellationToken), $"[ShopService][GetCompanyVatByBillNumber] Error occured while getting company vat by bll number: {billNumber}.");
		}

		public async Task<CompanyVat> GetCompanyVatById(string companyVatId, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetCompanyVatById] Getting CompanyVat by CompanyVat id: {companyVatId}.");
			return await this.Execute(() => this._repository.Table<CompanyVat>().Include(x => x.Company).FirstOrDefaultAsync(x => x.Id == companyVatId, cancellationToken), $"[ShopService][GetCompanyVatById] Error occured while getting company vat by company vat id: {companyVatId}.");
		}

		public async Task<List<CompanyVat>> GetCompanyVats(CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][GetCompanies] Getting companies.");

			return await this.Execute(() =>
			{
				var companyVats = this._repository.Table<CompanyVat>()
					.Include(x => x.Company)
					.OrderBy(x => x.GroupName);

				if (this.IsSqlite())
				{
					return companyVats
					.ThenByDescending(x => x.CreationDate)
					.ToListAsync(cancellationToken);
				}

				return companyVats.ToListAsync(cancellationToken);
			},
				$"[ShopService][GetCompanyVats] Error occured while getting company vats.");
		}

		public async Task<CompanyVat> AddCompanyVat(CompanyVat companyVat, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][AddCompanyVat] Adding CompanyVat: {JsonConvert.SerializeObject(companyVat)}.");

			var exisitingCompany = await this._repository.GetAsync<Company>(x => companyVat.Company.TaxTypeNumber == x.TaxTypeNumber, cancellationToken);

			if (exisitingCompany != null)
			{
				companyVat.Company = null;
				companyVat.CompanyId = exisitingCompany.Id;
			}

			return await this._repository.AddAsync(companyVat, cancellationToken);
		}

		public async Task<CompanyVat> UpdateCompanyVat(string id, CompanyVat companyVat, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][UpdateCompanyVat] Updating CompanyVat: {JsonConvert.SerializeObject(companyVat)}.");

			var exisitingCompany = await this._repository.GetAsync<Company>(x => companyVat.Company.TaxTypeNumber == x.TaxTypeNumber, cancellationToken);

			if (exisitingCompany != null)
			{
				companyVat.Company = null;
				companyVat.CompanyId = exisitingCompany.Id;
			}

			return await this._repository.UpdateAsync(id, companyVat, cancellationToken);
		}

		public async Task<CompanyVat> DeleteCompanyVatById(string id, CancellationToken cancellationToken = default)
		{
			this.ThrowIfDisposed();
			this._logger.LogInformation($"[ShopService][DeleteCompanyVatById] Deleting CompanyVat by id: {id}.");
			return await this._repository.DeleteAsync<CompanyVat>(id, cancellationToken);
		}

		#endregion Company VAT

		#region Sold Logs

		public async Task<List<SoldLog>> GetLogsBySoldId(string soldId, CancellationToken cancellationToken = default)
		{
			this._logger.LogInformation($"[ShopService][GetLogsBySoldId] Getting sold logs by sold id: {soldId}.");

			return (await this._repository.GetsAsync<SoldLog>(cancellationToken, x => x.SoldId == soldId)).ToList();
		}
		
		public async Task<SoldLog> DeleteSoldLogById(string id, CancellationToken cancellationToken = default)
		{
			this._logger.LogInformation($"[ShopService][DeleteSoldLogById] Deleting sold log by id: {id}.");

			return await this._repository.DeleteAsync<SoldLog>(id, cancellationToken);
		}

		#endregion Sold Logs

		#region Dispose Methods

		public void Dispose(bool disposing)
		{
			if (disposing && !disposed)
			{
				this._repository.Dispose();
				disposed = true;
			}
		}

		public void Dispose()
		{
			this._logger.LogInformation($"[ShopService][Dispose] Disposing: {JsonConvert.SerializeObject(this)}.");

			this.Dispose(disposing: true);
			GC.SuppressFinalize(this);
		}

		#endregion Dispose Methods

		#region Helper Methods

		private void ThrowIfDisposed()
		{
			if (disposed)
			{
				this._logger.LogInformation($"[ShopService][ThrowIfDisposed]");

				throw new ObjectDisposedException(GetType().Name);
			}
		}

		private async Task<TResult> Execute<TResult>(Func<Task<TResult>> func, string errorMessage)
		{
			try
			{
				return await func();
			}
			catch (DbException ex)
			{
				this._logger.LogError($"DbException - { errorMessage }", ex);
			}
			catch (DbUpdateConcurrencyException ex)
			{
				this._logger.LogError($"DbUpdateConcurrencyException - { errorMessage }", ex);
			}
			catch (DbUpdateException ex)
			{
				this._logger.LogError($"DbUpdateException - { errorMessage }", ex);
			}
			catch (Exception ex)
			{
				this._logger.LogError($"Exception - { errorMessage }", ex);
			}

			return default;
		}

		private bool IsSqlite()
		{
			return this._repository.Context.Database.ProviderName == "Microsoft.EntityFrameworkCore.Sqlite";
		}
		#endregion Helper Methods
	}
}
