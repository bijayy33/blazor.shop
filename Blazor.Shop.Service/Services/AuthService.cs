﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

using Blazor.Shop.DependencyInjection.Commons.Constants;
using Blazor.Shop.DependencyInjection.Models;
using Blazor.Shop.DependencyInjection.Services;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

using Newtonsoft.Json;

namespace Blazor.Shop.Service.Services
{
	public class AuthService : IAuthService
	{
		private readonly IConfiguration config;
		private readonly UserManager<ShopUser> userManager;
		private readonly IHttpContextAccessor httpContextAccessor;
		private readonly ILogger<AuthService> logger;

		public AuthService(
			IConfiguration config,
			UserManager<ShopUser> userManager,
			IHttpContextAccessor httpContextAccessor,
			ILogger<AuthService> logger)
		{
			this.config = config;
			this.userManager = userManager;
			this.httpContextAccessor = httpContextAccessor;
			this.logger = logger;
		}
		public string GetJwtToken()
		{
			try
			{
				var jwtObj = this.httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == Default.AccessToken);
				return jwtObj != null ? jwtObj.Value : null;
			}
			catch (Exception ex)
			{
				this.logger.LogError($"[{nameof(AuthService)}{nameof(GetJwtToken)}] Error occured while getting token.", ex);
				return null;
			}
		}

		public async Task<ShopUser> GetAuthenticatedUserAsync()
		{
			try
			{
				return await this.userManager.GetUserAsync(this.httpContextAccessor.HttpContext.User);
			}
			catch (Exception ex)
			{
				this.logger.LogError($"[{nameof(AuthService)}{nameof(GetAuthenticatedUserAsync)}] Error occured while getting authenticated user.", ex);
				return null;
			}
		}


		/// <summary>
		/// Generate JWT for specific user which include user claims
		/// </summary>
		/// <param name="user">application user who can login to the system</param>
		/// <returns>JWT, which will be on string type.</returns>
		public async Task<string> GenerateJWTAsync(ShopUser user)
		{
			try
			{
				if (user == null)
					user = this.GetAuthenticatedUserAsync().Result;

				IList<Claim> claims = await userManager.GetClaimsAsync(user);
				claims.Add(new Claim(ClaimTypes.Email, user.Email));
				claims.Add(new Claim(ClaimTypes.NameIdentifier, user.UserName));
				claims.Add(new Claim(ClaimTypes.Role, (await userManager.GetRolesAsync(user))[0]));

				var token = new JwtSecurityToken
				(
						issuer: config.GetSection("JWT:Issuer").Value,
						audience: config.GetSection("JWT:Audience").Value,
						expires: DateTime.UtcNow.AddSeconds(30),
						notBefore: DateTime.UtcNow,
						signingCredentials: new SigningCredentials(
							 new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.GetSection("JWT:SigningKey").Value)), SecurityAlgorithms.HmacSha256)
				 );
				return new JwtSecurityTokenHandler().WriteToken(token);
			}
			catch (Exception ex)
			{
				this.logger.LogError($"[{nameof(AuthService)}{nameof(GenerateJWTAsync)}] Error occured while generating token for user: {JsonConvert.SerializeObject(user)}.", ex);
				return null;
			}
		}

		/// <summary>
		/// Get claim principles out of expired access token
		/// </summary>
		/// <param name="token">expired access token</param>
		/// <returns>claim principle which will have user login information like username, claims, roles etc.</returns>
		public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
		{
			try
			{
				var tokenValidationParameters = new TokenValidationParameters
				{
					ValidateActor = true,
					ValidateAudience = true,
					ValidateLifetime = false,
					ValidateIssuerSigningKey = true,
					ValidIssuer = config.GetSection("JWT:Issuer").Value,
					ValidAudience = config.GetSection("JWT:Audience").Value,
					IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.GetSection("JWT:SigningKey").Value)),
				};

				SecurityToken securityToken;
				JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
				ClaimsPrincipal principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
				JwtSecurityToken jwtSecurityToken = securityToken as JwtSecurityToken;

				if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
					throw new SecurityTokenException("Invalid token");

				return principal;
			}
			catch (Exception ex)
			{
				this.logger.LogError($"[{nameof(AuthService)}{nameof(GetPrincipalFromExpiredToken)}] Error occured while generating token: {token}.", ex);
				return null;
			}
		}

		public string GeneratePassword(PasswordOptions options = default)
		{
			try
			{
				if (options == null)
				{
					options = new PasswordOptions()
					{
						RequiredLength = 6,
						RequiredUniqueChars = 4,
						RequireDigit = true,
						RequireLowercase = true,
						RequireNonAlphanumeric = true,
						RequireUppercase = true
					};
				}

				string[] randomChars = new[] {
						"ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase 
						"abcdefghijkmnopqrstuvwxyz",    // lowercase
						"0123456789",                   // digits
						"!@$?_-"                        // non-alphanumeric
						};

				Random rand = new Random(Environment.TickCount);
				List<char> chars = new List<char>();

				if (options.RequireUppercase)
					chars.Insert(rand.Next(0, chars.Count),
							randomChars[0][rand.Next(0, randomChars[0].Length)]);

				if (options.RequireLowercase)
					chars.Insert(rand.Next(0, chars.Count),
							randomChars[1][rand.Next(0, randomChars[1].Length)]);

				if (options.RequireDigit)
					chars.Insert(rand.Next(0, chars.Count),
							randomChars[2][rand.Next(0, randomChars[2].Length)]);

				if (options.RequireNonAlphanumeric)
					chars.Insert(rand.Next(0, chars.Count),
							randomChars[3][rand.Next(0, randomChars[3].Length)]);

				for (int i = chars.Count; i < options.RequiredLength
						|| chars.Distinct().Count() < options.RequiredUniqueChars; i++)
				{
					string rcs = randomChars[rand.Next(0, randomChars.Length)];
					chars.Insert(rand.Next(0, chars.Count),
							rcs[rand.Next(0, rcs.Length)]);
				}

				return new string(chars.ToArray());
			}
			catch (Exception ex)
			{
				this.logger.LogError($"[{nameof(AuthService)}{nameof(GeneratePassword)}] Error occured while generating password against: {JsonConvert.SerializeObject(options)}.", ex);
				return null;
			}
}
	}
}
