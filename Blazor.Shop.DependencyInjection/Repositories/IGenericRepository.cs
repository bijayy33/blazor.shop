﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.DependencyInjection.Models;

using Microsoft.EntityFrameworkCore;

namespace Blazor.Shop.DependencyInjection.Repositories
{
	public interface IGenericRepository : IDisposable
	{
		//Context
		AppDbContext Context { get; }
		IQueryable<TEntity> Table<TEntity>() where TEntity : class, new();

		/// <exception cref="ArgumentNullException" />
		Task<TEntity> GetAsync<TEntity>(string id, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new();

		Task<TEntity> GetAsync<TEntity>(Expression<Func<TEntity, bool>> expression, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new();
		Task<IEnumerable<TEntity>> GetsAsync<TEntity>(CancellationToken cancellationToken = default, Expression<Func<TEntity, bool>> expression = null) where TEntity : BaseEntity, new();

		/// <exception cref="ArgumentNullException" />
		Task<TEntity> AddAsync<TEntity>(TEntity entity, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new();

		/// <exception cref="ArgumentNullException" />
		Task<IEnumerable<TEntity>> AddMultipleAsync<TEntity>(List<TEntity> entities, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new();

		/// <exception cref="ArgumentNullException" />
		Task<TEntity> UpdateAsync<TEntity>(string id, TEntity entity, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new();

		/// <exception cref="ArgumentNullException" />
		Task<IEnumerable<TEntity>> UpdateMultipleAsync<TEntity>(List<TEntity> entities, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new();

		/// <exception cref="ArgumentNullException" />
		Task<TEntity> DeleteAsync<TEntity>(string id, CancellationToken cancellationToken = default) where TEntity : BaseEntity, new();

		/// <summary>
		///Use this when you want to explicityly save the changes when accessed the <see cref="Table{TEntity}"/> and <seealso cref="Context"/>.
		/// </summary>
		/// <exception cref="ArgumentNullException" />
		Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
	}
}
