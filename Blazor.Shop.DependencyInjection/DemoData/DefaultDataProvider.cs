﻿using System.Collections.Generic;
using Blazor.Shop.DependencyInjection.Common.Constants;
using Blazor.Shop.DependencyInjection.Models;

using Microsoft.AspNetCore.Identity;

namespace Blazor.Shop.DependencyInjection.DemoData
{
	public class DefaultDataProvider
	{
		private static PasswordHasher<ShopUser> passwordHash = new PasswordHasher<ShopUser>();

		public static List<Product> Products
			=> new List<Product>
			{
				new Product
				{
					Id = DefaultProduct.ProductId1,
					Name = "Gulf",
					Code = "Gulf Mobil",
					Description = "Gulf Mobil",
					AvailableQuantity = 5,
					Size = "10L",
					EncodedMrp = "4000",
					EncodedCostPrice = "4000",
					EncodedSellingPrice = "4100",
				},
				new Product
				{
					Id = DefaultProduct.ProductId2,
					Name = "Petro Gulf",
					Code = "Petro Gulf Mobil",
					Description = "Petro Gulf Mobil",
					AvailableQuantity = 5,
					Size = "10L",
					EncodedMrp = "3800",
					EncodedCostPrice = "3800",
					EncodedSellingPrice = "3900",
				}
				,
				new Product
				{
					Id = DefaultProduct.ProductId3,
					Name = "Petro Gulf",
					Code = "Petro Gulf Mobil",
					Description = "Petro Gulf Mobil",
					AvailableQuantity = 5,
					Size = "5L",
					EncodedMrp = "1300",
					EncodedCostPrice = "1500",
					EncodedSellingPrice = "1500",
				},
				new Product
				{
					Id = DefaultProduct.ProductId4,
					Name = "Petro Gulf",
					Code = "Petro Gulf Mobil",
					Description = "Petro Gulf Mobil",
					AvailableQuantity = 5,
					Size = "1L",
					EncodedMrp = "300",
					EncodedCostPrice = "300",
					EncodedSellingPrice = "390",
				}
			};

		//public static List<Sold> SoldProducts
		//	=> new List<Sold>
		//	{
		//		new Sold
		//		{
		//			ProductId = DefaultProduct.ProductId1,
		//			Quantity = 1,
		//			BuyerName = DefaultUser.SubAdminEmail,
		//			BuyerPhones = "0987654321",
		//			TotalAmount = 380,
		//			PaidAmount = 380,
		//			PendingAmount = 0,
		//			DiscountAmount = 0,
		//			SellerName = "Ajay Yadav"
		//		},
		//		new Sold
		//		{
		//			ProductId = DefaultProduct.ProductId2,
		//			Quantity = 1,
		//			BuyerName = DefaultUser.ManagerEmail,
		//			BuyerPhones = "9876543210",
		//			TotalAmount = 380,
		//			PaidAmount = 380,
		//			PendingAmount = 0,
		//			DiscountAmount = 0,
		//			SellerName = "Jhagaru Yadav"
		//		},
		//		new Sold
		//		{
		//			ProductId = DefaultProduct.ProductId3,
		//			Quantity = 1,
		//			BuyerName = DefaultUser.ManagerEmail,
		//			BuyerPhones = "9876543210",
		//			TotalAmount = 380,
		//			PaidAmount = 380,
		//			PendingAmount = 0,
		//			DiscountAmount = 0,
		//			SellerName = "Jhagaru Yadav"
		//		},
		//		new Sold
		//		{
		//			ProductId = DefaultProduct.ProductId4,
		//			Quantity = 1,
		//			BuyerName = DefaultUser.AdminEmail,
		//			BuyerPhones = "0987654321",
		//			TotalAmount = 380,
		//			PaidAmount = 270,
		//			PendingAmount = 100,
		//			DiscountAmount = 10,
		//			SellerName = "Ajay Yadav"
		//		}
		//	};

		//public static List<SymbolicPrice> SymbolicPrices
		//	=> new List<SymbolicPrice> {
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'A',
		//			Value = 0,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'B',
		//			Value = 1,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'C',
		//			Value = 2,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'D',
		//			Value = 3,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'E',
		//			Value = 4,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'F',
		//			Value = 5,

		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'G',
		//			Value = 6,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'H',
		//			Value = 7,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'I',
		//			Value = 8,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'J',
		//			Value = 9,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'K',
		//			Value = 10,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'L',
		//			Value = 11,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'M',
		//			Value = 12,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'N',
		//			Value = 13,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'O',
		//			Value = 14,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'P',
		//			Value = 15,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'Q',
		//			Value = 16,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'R',
		//			Value = 17,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'S',
		//			Value = 18,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'T',
		//			Value = 19,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'U',
		//			Value = 20,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'V',
		//			Value = 21,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'W',
		//			Value = 22,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'X',
		//			Value = 23,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'Y',
		//			Value = 24,
		//		},
		//		new SymbolicPrice
		//		{
		//			FiscalYear = "2077",
		//			Alphabet = 'Z',
		//			Value = 25,
		//		}
		//	};

		//public static List<IdentityRole> Roles
		//	=> new List<IdentityRole>
		//	{
		//	new IdentityRole
		//		{
		//			Id = DefaultRole.SuperAdminRoleId,
		//			Name = DefaultRole.SuperAdmin,
		//			NormalizedName = DefaultRole.SuperAdmin.ToUpperInvariant(),
		//		},
		//		new IdentityRole
		//		{
		//			Id = DefaultRole.AdminRoleId,
		//			Name = DefaultRole.Admin,
		//			NormalizedName = DefaultRole.Admin.ToUpperInvariant(),
		//		},
		//		new IdentityRole
		//		{
		//			Id = DefaultRole.SubAdminRoleId,
		//			Name = DefaultRole.SubAdmin,
		//			NormalizedName = DefaultRole.SubAdmin.ToUpperInvariant(),
		//		},
		//		new IdentityRole
		//		{
		//			Id = DefaultRole.ManagerRoleId,
		//			Name = DefaultRole.Manager,
		//			NormalizedName = DefaultRole.Manager.ToUpperInvariant(),
		//		},
		//		new IdentityRole
		//		{
		//			Id = DefaultRole.UserRoleId,
		//			Name = DefaultRole.User,
		//			NormalizedName = DefaultRole.User.ToUpperInvariant(),
		//		},
		//		new IdentityRole
		//		{
		//			Id = DefaultRole.CompanyRoleId,
		//			Name = DefaultRole.Company,
		//			NormalizedName = DefaultRole.Company.ToUpperInvariant(),
		//		},
		//		new IdentityRole
		//		{
		//			Id = DefaultRole.DistributerRoleId,
		//			Name = DefaultRole.Distributer,
		//			NormalizedName = DefaultRole.Distributer.ToUpperInvariant(),
		//		},
		//		new IdentityRole
		//		{
		//			Id = DefaultRole.ResellerRoleId,
		//			Name = DefaultRole.Reseller,
		//			NormalizedName = DefaultRole.Reseller.ToUpperInvariant(),
		//		}
		//	};

		//public static List<ShopUser> Users
		//	=> new List<ShopUser>
		//	{
		//		new ShopUser()
		//		{
		//			Id = DefaultUser.SuperAdminId,
		//			UserName = DefaultUser.SuperAdminEmail,
		//			Email = DefaultUser.SuperAdminEmail,
		//			EmailConfirmed = true,
		//			PasswordHash = passwordHash.HashPassword(null, DefaultUser.Password),
		//		},
		//		new ShopUser()
		//		{
		//			Id = DefaultUser.AdminId,
		//			UserName = DefaultUser.AdminEmail,
		//			Email = DefaultUser.AdminEmail,
		//			EmailConfirmed = true,
		//			PasswordHash = passwordHash.HashPassword(null, DefaultUser.Password),
		//		},
		//		new ShopUser()
		//		{
		//			Id = DefaultUser.SubAdminId,
		//			UserName = DefaultUser.SubAdminEmail,
		//			Email = DefaultUser.SubAdminEmail,
		//			EmailConfirmed = true,
		//			PasswordHash = passwordHash.HashPassword(null, DefaultUser.Password),
		//		},
		//		new ShopUser()
		//		{
		//			Id = DefaultUser.ManagerId,
		//			UserName = DefaultUser.ManagerEmail,
		//			Email = DefaultUser.ManagerEmail,
		//			EmailConfirmed = true,
		//			PasswordHash = passwordHash.HashPassword(null, DefaultUser.Password),
		//		},
		//		new ShopUser()
		//		{
		//			Id = DefaultUser.UserId,
		//			UserName = DefaultUser.UserEmail,
		//			Email = DefaultUser.UserEmail,
		//			EmailConfirmed = true,
		//			PasswordHash = passwordHash.HashPassword(null, DefaultUser.Password),
		//		},
		//		new ShopUser()
		//		{
		//			Id = DefaultUser.CompanyId,
		//			UserName = DefaultUser.CompanyEmail,
		//			Email = DefaultUser.CompanyEmail,
		//			EmailConfirmed = true,
		//			PasswordHash = passwordHash.HashPassword(null, DefaultUser.Password),
		//		},
		//		new ShopUser()
		//		{
		//			Id = DefaultUser.DistributerId,
		//			UserName = DefaultUser.DistributerEmail,
		//			Email = DefaultUser.DistributerEmail,
		//			EmailConfirmed = true,
		//			PasswordHash = passwordHash.HashPassword(null, DefaultUser.Password),
		//		},
		//		new ShopUser()
		//		{
		//			Id = DefaultUser.ResellerId,
		//			UserName = DefaultUser.ResellerEmail,
		//			Email = DefaultUser.ResellerEmail,
		//			EmailConfirmed = true,
		//			PasswordHash = passwordHash.HashPassword(null, DefaultUser.Password),
		//		}
		//	};

		//public static List<IdentityUserRole<string>> UserRoles
		//	=> new List<IdentityUserRole<string>>
		//	{
		//		new IdentityUserRole<string>
		//		{
		//			UserId = DefaultUser.SuperAdminId,
		//			RoleId = DefaultRole.SuperAdminRoleId
		//		},
		//		new IdentityUserRole<string>
		//		{
		//			UserId = DefaultUser.AdminId,
		//			RoleId = DefaultRole.AdminRoleId
		//		},
		//		new IdentityUserRole<string>
		//		{
		//			UserId = DefaultUser.SubAdminId,
		//			RoleId = DefaultRole.SubAdminRoleId
		//		},
		//		new IdentityUserRole<string>
		//		{
		//			UserId = DefaultUser.ManagerId,
		//			RoleId = DefaultRole.ManagerRoleId
		//		},
		//		new IdentityUserRole<string>
		//		{
		//			UserId = DefaultUser.UserId,
		//			RoleId = DefaultRole.UserRoleId
		//		},
		//		new IdentityUserRole<string>
		//		{
		//			UserId = DefaultUser.CompanyId,
		//			RoleId = DefaultRole.CompanyRoleId
		//		},
		//		new IdentityUserRole<string>
		//		{
		//			UserId = DefaultUser.DistributerId,
		//			RoleId = DefaultRole.DistributerRoleId
		//		},
		//		new IdentityUserRole<string>
		//		{
		//			UserId = DefaultUser.ResellerId,
		//			RoleId = DefaultRole.ResellerRoleId
		//		}
		//	};
	}
}
