﻿using System.Security.Claims;
using System.Threading.Tasks;

using Blazor.Shop.DependencyInjection.Models;

using Microsoft.AspNetCore.Identity;

namespace Blazor.Shop.DependencyInjection.Services
{
	public interface IAuthService
	{
		string GetJwtToken();
		Task<ShopUser> GetAuthenticatedUserAsync();
		Task<string> GenerateJWTAsync(ShopUser user = null);
		ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
		string GeneratePassword(PasswordOptions passwordOptions = default);
	}
}
