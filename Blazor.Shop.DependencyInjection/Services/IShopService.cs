﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.DependencyInjection.Models;
using Blazor.Shop.DependencyInjection.Models.Dtos;

namespace Blazor.Shop.DependencyInjection.Services
{
	public interface IShopService: IDisposable
	{
		#region Products

		Task<ProductDto> GetProductAsync(string id, CancellationToken cancellationToken = default);
		Task<IEnumerable<ProductDto>> GetProductsAsync(CancellationToken cancellationToken = default);
		Task<ProductDto> AddProductAsync(ProductDto productDto, CancellationToken cancellationToken = default);
		Task<IEnumerable<ProductDto>> AddProductsAsync(IEnumerable<ProductDto> productDtos, CancellationToken cancellationToken = default);
		Task<ProductDto> UpdateProductAsync(string id, ProductDto productDto, CancellationToken cancellationToken = default);
		Task<ProductDto> DeleteProductAsync(string id, CancellationToken cancellationToken = default);

		#endregion Products

		#region Sold Products

		Task<IEnumerable<Sold>> GetSoldListAsync(CancellationToken cancellationToken = default);
		Task<Sold> GetSoldByIdAsync(string soldId, CancellationToken cancellationToken = default);
		Task<IEnumerable<Sold>> GetSoldsByUserIdAsync(string userId, CancellationToken cancellationToken = default);
		Task<Sold> AddSoldsPerUserAsync(Sold soldProduct, CancellationToken cancellationToken = default);
		Task<Sold> UpdateSoldProductAsync(string soldId, Sold soldProductDto, CancellationToken cancellationToken = default);
		Task<Sold> DeleteSoldById(string soldId, CancellationToken cancellationToken = default);
		Task<SoldProduct> DeleteSoldProductById(string soldProductId, CancellationToken cancellationToken = default);
		#endregion Sold Products

		#region Product VATs

		Task<IEnumerable<Vat>> GetVatsAsync(CancellationToken cancellationToken = default);
		Task<Vat> GetVatByInvoiceNumberAsync(long invoiceNumber, CancellationToken cancellationToken = default);
		Task<Vat> AddVatAsync(Vat vat, CancellationToken cancellationToken = default);

		#endregion Product VATs

		#region User Product Report

		Task<UserProductReportDto> GetUserProductsReportAsync(string phoneNumber, CancellationToken cancellationToken = default);
		Task<IEnumerable<UserPurchaseDto>> GetUserPurchaseReportAsync(CancellationToken cancellationToken = default);

		#endregion User Product Report

		#region Symbolic Price
		Task<SymbolicPrice> GetSymbolicPriceAsync(char alphabet, CancellationToken cancellationToken = default);
		Task<decimal> GetPriceAsync(string symbolicPrice, CancellationToken cancellationToken = default);
		Task<IEnumerable<SymbolicPrice>> GetSymbolicPricesAsync(CancellationToken cancellationToken = default);
		Task<SymbolicPrice> AddSymbolicPriceAsync(SymbolicPrice symbolicPrice, CancellationToken cancellationToken = default);
		Task<SymbolicPrice> UpdateSymbolicPriceAsync(string id, SymbolicPrice symbolicPrice, CancellationToken cancellationToken = default);
		Task<SymbolicPrice> DeleteSymbolicPriceAsync(string id, CancellationToken cancellationToken = default);

		#endregion Symbolic Price

		#region Company
		Task<Company> GetCompanyByTaxTypeNumber(string taxTypeNumber, CancellationToken cancellationToken = default);
		Task<Company> GetCompanyById(string companyId, CancellationToken cancellationToken = default);
		Task<List<Company>> GetCompanies(CancellationToken cancellationToken = default);
		Task<Company> AddCompany(Company company, CancellationToken cancellationToken = default);
		Task<Company> UpdateCompany(string companyId, Company company, CancellationToken cancellationToken = default);
		Task<Company> DeleteCompanyById(string companyId, CancellationToken cancellationToken = default);

		#endregion Company

		#region Company VAT

		Task<CompanyVat> GetCompanyVatByBillNumber(string CompanyVatId, CancellationToken cancellationToken = default);
		Task<CompanyVat> GetCompanyVatById(string CompanyVatId, CancellationToken cancellationToken = default);
		Task<List<CompanyVat>> GetCompanyVats(CancellationToken cancellationToken = default);
		Task<CompanyVat> AddCompanyVat(CompanyVat companyVat, CancellationToken cancellationToken = default);
		Task<CompanyVat> UpdateCompanyVat(string id, CompanyVat companyVat, CancellationToken cancellationToken = default);
		Task<CompanyVat> DeleteCompanyVatById(string id, CancellationToken cancellationToken = default);

		#endregion Company VAT

		#region Sold Logs

		Task<List<SoldLog>> GetLogsBySoldId(string soldId, CancellationToken cancellationToken = default);
		
		Task<SoldLog> DeleteSoldLogById(string logId, CancellationToken cancellationToken = default);

		#endregion Sold Logs

		void Dispose(bool disposing);
	}
}
