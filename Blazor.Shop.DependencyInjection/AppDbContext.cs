﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Loader;

using Blazor.Shop.DependencyInjection.Models;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Blazor.Shop.DependencyInjection
{
	public abstract class AppDbContext : IdentityDbContext<ShopUser>
	{
		public AppDbContext() : base()
		{
			
		}

		public AppDbContext(DbContextOptions options) : base(options)
		{
		}
	}
}
