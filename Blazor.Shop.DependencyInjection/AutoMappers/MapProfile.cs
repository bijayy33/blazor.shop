﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;

using Blazor.Shop.DependencyInjection.Models;
using Blazor.Shop.DependencyInjection.Models.Dtos;
using Blazor.Shop.DependencyInjection.Resolvers;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Blazor.Shop.DependencyInjection.AutoMappers
{
	public class MapProfile : Profile
	{
		public MapProfile()
		{
			CreateMap<ShopUser, UserDto>()
				.ForMember(userDto => userDto.UserName, userConfig => userConfig.MapFrom(x => string.IsNullOrEmpty(x.UserName) ? x.PhoneNumber : x.UserName.ToUpperInvariant().Trim()))
				.ForMember(userDto => userDto.Role, userConfig => userConfig.MapFrom(x => this.GetRoles(x.Id)));

			CreateMap<UserDto, ShopUser>()
				.ForMember(shopUser => shopUser.UserName, userConfig => userConfig.MapFrom(x => string.IsNullOrEmpty(x.UserName) ? x.PhoneNumber : x.UserName.ToUpperInvariant().Trim()));

			CreateMap<Product, ProductDto>()
				.ForMember(productDto => productDto.Code, productConfig => productConfig.MapFrom(x => x.Code.Trim().ToUpperInvariant()))
				.ForMember(productDto => productDto.Description, productConfig => productConfig.MapFrom(x => x.Description.Trim().ToUpperInvariant()))
				.ForMember(productDto => productDto.EncodedMrp, productConfig => productConfig.MapFrom(x => string.IsNullOrWhiteSpace(x.EncodedMrp) ? x.EncodedCostPrice.Trim() : x.EncodedMrp.Trim()))
				.ForMember(productDto => productDto.EncodedCostPrice, productConfig => productConfig.MapFrom(x => string.IsNullOrWhiteSpace(x.EncodedCostPrice) ? x.EncodedMrp.Trim() : x.EncodedCostPrice.Trim()));

			CreateMap<Product, ProductDto>()
				.ForMember(productDto => productDto.Code, productConfig => productConfig.MapFrom(x => x.Code.Trim().ToUpperInvariant()))
				.ForMember(productDto => productDto.Description, productConfig => productConfig.MapFrom(x => x.Description.Trim().ToUpperInvariant()))
				.ForMember(productDto => productDto.EncodedMrp, productConfig => productConfig.MapFrom(x => string.IsNullOrWhiteSpace(x.EncodedMrp) ? x.EncodedCostPrice.Trim() : x.EncodedMrp.Trim()))
				.ForMember(productDto => productDto.EncodedCostPrice, productConfig => productConfig.MapFrom(x => string.IsNullOrWhiteSpace(x.EncodedCostPrice) ? x.EncodedMrp.Trim() : x.EncodedCostPrice.Trim()))
				.ForMember(productDto => productDto.Mrp, productConfig => productConfig.MapFrom(x => string.IsNullOrWhiteSpace(x.EncodedMrp) ? this.GetPriceAsync(x.EncodedCostPrice.Trim() ?? string.Empty).Result : this.GetPriceAsync(x.EncodedMrp.Trim() ?? string.Empty).Result))
				.ForMember(productDto => productDto.CostPrice, productConfig => productConfig.MapFrom(x => string.IsNullOrWhiteSpace(x.EncodedCostPrice) ? this.GetPriceAsync(x.EncodedMrp.Trim() ?? string.Empty).Result : this.GetPriceAsync(x.EncodedCostPrice.Trim() ?? string.Empty).Result))
				.ForMember(productDto => productDto.SellingPrice, productConfig => productConfig.MapFrom(x => this.GetPriceAsync(x.EncodedSellingPrice ?? string.Empty).Result))
				.ReverseMap();

			CreateMap<Sold, SoldLog>()
				.ForMember(soldLog => soldLog.TotalDiscountAmount, sold => sold.MapFrom(x => x.TotalDiscountAmount))
				.ForMember(soldLog => soldLog.TotalAmount, sold => sold.MapFrom(x => x.TotalAmount))
				.ForMember(soldLog => soldLog.TotalPaidAmount, sold => sold.MapFrom(x => x.TotalPaidAmount))
				.ForMember(soldLog => soldLog.TotalPendingAmount, sold => sold.MapFrom(x => x.TotalPendingAmount));
		}

		private string GetRoles(string userId)
		{
			AppDbContext context = new CustomActivator().GetInstance<AppDbContext>();

			var userRoles = context.UserRoles.Where(x => x.UserId == userId);
			var roles = context.Roles.Where(x => userRoles.Any(y => y.RoleId == x.Id)).Select(x => x.Name);

			return string.Join(',', roles);
		}

		private async Task<decimal> GetPriceAsync(string symbolicPrice)
		{
			decimal value = -1;
			string result = string.Empty;
			bool notNumber = false;

			try
			{
				if (!decimal.TryParse(symbolicPrice, out value))
				{
					AppDbContext context = new CustomActivator().GetInstance<AppDbContext>();

					IEnumerable<SymbolicPrice> symbolicPrices = await context.Set<SymbolicPrice>().ToListAsync();

					if (!symbolicPrices.First().IsCancelled)
					{
						value = 0;

						foreach (char c in symbolicPrice)
						{
							var price = symbolicPrices.FirstOrDefault(p => p.Alphabet == c);
							result += price == null ? c + string.Empty : price.Value + string.Empty;
						}

						notNumber = true;
					}
				}

				if (notNumber)
				{
					decimal.TryParse(result, out value);
				}
			}
			catch (Exception ex)
			{
			}

			return value;
		}
	}
}
