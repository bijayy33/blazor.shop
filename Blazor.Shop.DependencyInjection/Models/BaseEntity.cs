﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blazor.Shop.DependencyInjection.Models
{
	public class BaseEntity
	{
		public virtual string Id { get; set; }
		public virtual bool IsDeleted { get; set; }

		[NotMapped] //Just used to check whether the cancellation is done.
		public virtual bool IsCancelled { get; set; }
		public virtual string CreatedById { get; set; }
		public virtual DateTimeOffset? CreationDate { get; set; } = DateTimeOffset.Now;
		public virtual string ChangedById { get; set; }
		public virtual DateTimeOffset? ChangedDate { get; set; }
	}
}
