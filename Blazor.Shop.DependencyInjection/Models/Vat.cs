﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Blazor.Shop.DependencyInjection.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blazor.Shop.DependencyInjection.Models
{
	public class Vat : BaseEntity
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public virtual long InvoiceNumber { get; set; }
		public virtual decimal VatPercentage { get; set; }
		public virtual decimal TotalTaxableAmount { get; set; }
		public virtual decimal TotalVatAmount { get; set; }
		public virtual decimal TotalAmount { get; set; }
		public virtual List<ProductVat> ProductVats { get; set; }
	}

	public class VatConfiguration : IEntityTypeConfiguration<Vat>
	{
		public void Configure(EntityTypeBuilder<Vat> builder)
		{
			builder.ToTable(nameof(Vat));
			builder.HasKey(c => c.InvoiceNumber);
			builder.Property(c => c.VatPercentage).IsRequired();
			builder.Property(c => c.TotalTaxableAmount).IsRequired();
			builder.Property(c => c.TotalVatAmount).IsRequired();
			builder.Property(c => c.TotalAmount).IsRequired();
			builder.Property(c => c.IsDeleted).HasDefaultValue(false).IsRequired();
			builder.Property(c => c.CreatedById).IsRequired(false);
			builder.Property(c => c.CreationDate).ValueGeneratedOnAdd().HasDefaultValue(DateTimeOffset.Now).IsRequired(false);
			builder.Property(c => c.ChangedById).IsRequired(false);
			builder.Property(c => c.ChangedDate).ValueGeneratedOnUpdate().HasDefaultValue(DateTimeOffset.Now).IsRequired(false);

			builder.Ignore(x => x.Id);
		}
	}
}
