﻿using System.ComponentModel.DataAnnotations;

namespace Blazor.Shop.DependencyInjection.Models.Dtos
{
	public class UserSignInDto
	{
		[Required]
		public string UserName { get; set; }
		[Required]
		public string Password { get; set; }
	}
}
