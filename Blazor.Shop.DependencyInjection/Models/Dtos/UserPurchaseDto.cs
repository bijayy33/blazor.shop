﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blazor.Shop.DependencyInjection.Models.Dtos
{
	/// <summary>
	/// Use this for showing overral report of users irrespective of products.
	/// i.e. Total product purchased, total amount paid, gross total amount, total discount, total pending amount.
	/// </summary>
	public class UserPurchaseDto
	{
		public string FullName { get; set; }
		public string Emails { get; set; }
		public string Phones { get; set; }
		public string Address { get; set; }
		/// <summary>
		/// Total amount of products purchased.
		/// </summary>
		public int Quantity { get; set; }
		public decimal TotalAmount { get; set; } //Total
		public decimal TotalAmountPaid { get; set; } //Total Paid
		public decimal TotalPendingAmount { get; set; }
		public decimal TotalDiscountAmount { get; set; }
		public bool OperationCancelled { get; set; }
	}
}
