﻿using System;
using System.Collections.Generic;

using Blazor.Shop.DependencyInjection.Common.Enums;

namespace Blazor.Shop.DependencyInjection.Models.Dtos
{
	public class ProductDto : BaseDto
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public string Code { get; set; }
		public int AvailableQuantity { get; set; }
		public QuantityType QuantityType { get; set; } = QuantityType.PCS;
		public int QuantityTypeValue { get; set; } = 1;
		public string[] QuantityTypes { get; set; } = Enum.GetNames(typeof(QuantityType));
		public string Size { get; set; } //5L, 10L, 5 meters
		public string EncodedMrp { get; set; } //Symbolic Mrp
		public string EncodedCostPrice { get; set; }
		public string EncodedSellingPrice { get; set; }
		public decimal Mrp { get; set; }
		public decimal CostPrice { get; set; }
		public decimal SellingPrice { get; set; }
		public decimal TotalSoldAmount { get; set; }
		public decimal TotalPendingAmount { get; set; }
		public decimal TotalDiscountAmount { get; set; }
	}
}
