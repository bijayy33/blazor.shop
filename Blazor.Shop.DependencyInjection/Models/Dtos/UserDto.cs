﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blazor.Shop.DependencyInjection.Models.Dtos
{
	public class UserDto : BaseDto
	{
		[Required]
		public string FullName { get; set; }
		[NotMapped]
		public string Role { get; set; }
		public string UserName { get; set; }
		public string Email { get; set; }
		[Required]
		public string PhoneNumber { get; set; }
		[Required]
		public string Address { get; set; }
	}
}
