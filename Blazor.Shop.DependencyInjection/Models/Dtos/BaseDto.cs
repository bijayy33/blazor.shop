﻿using System;

namespace Blazor.Shop.DependencyInjection.Models.Dtos
{
	public class BaseDto
	{
		public string Id { get; set; }
		public bool IsDeleted { get; set; }
		public bool IsCancelled { get; set; }
		public string CreatedById { get; set; }
		public DateTimeOffset? CreationDate { get; set; }
		public string ChangedById { get; set; }
		public DateTimeOffset? ChangedDate { get; set; }
	}
}
