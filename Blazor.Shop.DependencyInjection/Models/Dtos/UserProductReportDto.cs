﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Blazor.Shop.DependencyInjection.Models.Dtos
{
	/// <summary>
	/// Contains the information of user along with the products bought.
	/// </summary>
	public class UserProductReportDto
	{
		public UserProductReportDto()
		{
			this.UserProducts = new List<UserProductDto>();
		}
		public string FullName { get; set; }
		public string Emails { get; set; }
		public string Phones { get; set; }
		public string Address { get; set; }
		public bool IsCancelled { get; set; }
		public IEnumerable<UserProductDto> UserProducts { get; set; }
	}

	public class UserProductDto
	{
		public string ProductName { get; set; }
		public string ProductCode { get; set; }
		public int Quantity { get; set; } //Bought by user
		public decimal TotalAmount { get; set; }
		public decimal PaidAmount { get; set; }
		public decimal PendingAmount { get; set; }
		public decimal DiscountAmount { get; set; }
		public DateTimeOffset? PurchasedDate { get; set; } //Bought by user
	}
}
