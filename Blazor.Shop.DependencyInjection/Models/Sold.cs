﻿using System.Collections.Generic;

using Blazor.Shop.DependencyInjection.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blazor.Shop.DependencyInjection.Models
{
	/// <summary>
	/// This may contains the <see cref="Product"/> not available in product tables.
	/// Because the produdct description and code may not be availble.
	/// </summary>
	public class Sold : BaseEntity
	{
		public virtual string SellerId { get; set; } // Seller Id.
		public virtual string BuyerId { get; set; } // Seller Id.
		public virtual decimal TotalAmount { get; set; } // Gross Amount i.e. 2 * Selling Price
		public virtual decimal TotalPaidAmount { get; set; } //Amount Paid by customer.
		public virtual decimal TotalDiscountAmount { get; set; } //Discount given to customer.
		public virtual decimal TotalPendingAmount { get; set; } //Pending Amount to take from customer.
		public virtual List<SoldProduct> SoldProducts { get; set; }
		public virtual List<SoldLog> SoldLogs { get; set; }
		public virtual ShopUser Buyer { get; set; }
		public virtual ShopUser Seller { get; set; }
	}

	public class SoldProductConfiguration : EntityConfiguration<Sold>
	{
		public override void ConfigureEntity(EntityTypeBuilder<Sold> builder)
		{
			builder.ToTable(nameof(Sold));
			builder.Property(c => c.TotalAmount).HasDefaultValue(0).IsRequired();
			builder.Property(c => c.TotalPaidAmount).HasDefaultValue(0).IsRequired();
			builder.Property(c => c.TotalDiscountAmount).HasDefaultValue(0).IsRequired();
			builder.Property(c => c.TotalPendingAmount).HasDefaultValue(0).IsRequired();

			builder.HasOne(x => x.Seller)
				.WithOne()
				.HasForeignKey<Sold>(x => x.SellerId);

			builder.HasOne(x => x.Buyer)
				.WithOne()
				.HasForeignKey<Sold>(x => x.BuyerId);

			base.Configure(builder);
		}
	}
}
