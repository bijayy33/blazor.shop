﻿using Blazor.Shop.DependencyInjection.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blazor.Shop.DependencyInjection.Models
{
	public class CompanyVat : BaseEntity
	{
		public virtual string BillNumber { get; set; }
		public virtual decimal VatPercentage { get; set; }
		public virtual decimal PurchasedAmount { get; set; }
		public virtual decimal TaxAmount { get; set; }
		public virtual decimal AmountWithoutTax { get; set; }
		public virtual string GroupName { get; set; }
		public virtual string CompanyId { get; set; }
		public virtual Company Company { get; set; }
	}

	public class CompanyVatConfiguration : EntityConfiguration<CompanyVat>
	{
		public override void ConfigureEntity(EntityTypeBuilder<CompanyVat> builder)
		{
			builder.ToTable(nameof(CompanyVat));
			builder.HasIndex(c => new { c.BillNumber, c.CompanyId }).IsUnique();
			builder.Property(c => c.VatPercentage).IsRequired();
			builder.Property(c => c.PurchasedAmount).IsRequired();
			builder.Property(c => c.TaxAmount).IsRequired();
			builder.Property(c => c.AmountWithoutTax).IsRequired();
			builder.Property(c => c.GroupName).IsRequired();

			builder.HasOne(x => x.Company)
				.WithMany(x => x.CompanyVats)
				.HasForeignKey(x => x.CompanyId);

			base.Configure(builder);
		}
	}
}
