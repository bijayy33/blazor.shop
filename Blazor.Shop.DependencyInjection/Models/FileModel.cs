﻿using Blazor.Shop.DependencyInjection.Common.Enums;
using Blazor.Shop.DependencyInjection.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blazor.Shop.DependencyInjection.Models
{
	public class FileModel : BaseEntity
	{
		public string Name { get; set; }
		public string Base64String { get; set; }
		public byte[] Bytes { get; set; }
		public string Type { get; set; }
		public FileFor For { get; set; }
	}

	public class FileModelConfiguration : EntityConfiguration<FileModel>
	{
		public override void ConfigureEntity(EntityTypeBuilder<FileModel> builder)
		{
			builder.ToTable(nameof(FileModel));
			builder.Property(c => c.Name).IsRequired();
			builder.Property(c => c.Base64String).IsRequired();
			builder.Property(c => c.Bytes).IsRequired(false);
			builder.Property(c => c.Type).IsRequired();
			builder.Property(c => c.For).HasDefaultValue(FileFor.User).IsRequired();

			base.Configure(builder);
		}
	}
}
