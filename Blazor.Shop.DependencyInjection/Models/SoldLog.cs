﻿using Blazor.Shop.DependencyInjection.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blazor.Shop.DependencyInjection.Models
{
	public class SoldLog : BaseEntity
	{
		public virtual decimal TotalAmount { get; set; } // Gross Amount i.e. 2 * Selling Price
		public virtual decimal TotalPaidAmount { get; set; } //Amount Paid by customer.
		public virtual decimal TotalDiscountAmount { get; set; } //Discount given to customer.
		public virtual decimal TotalPendingAmount { get; set; } //Pending Amount to take from customer.
		public string SoldId { get; set; } // SoldProductId.
		public Sold Sold { get; set; } // Sold Product.
	}

	public class SoldLogConfiguration : EntityConfiguration<SoldLog>
	{
		public override void ConfigureEntity(EntityTypeBuilder<SoldLog> builder)
		{
			builder.ToTable(nameof(SoldLog));
			builder.Property(c => c.TotalAmount).IsRequired();
			builder.Property(c => c.TotalDiscountAmount).IsRequired();
			builder.Property(c => c.TotalPaidAmount).IsRequired();
			builder.Property(c => c.TotalPendingAmount).IsRequired();

			builder.HasOne(x => x.Sold)
				.WithMany(x => x.SoldLogs)
				.HasForeignKey(x => x.SoldId);

			base.Configure(builder);
		}
	}
}
