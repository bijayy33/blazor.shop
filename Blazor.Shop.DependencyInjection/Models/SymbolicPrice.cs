﻿using Blazor.Shop.DependencyInjection.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blazor.Shop.DependencyInjection.Models
{
	public class SymbolicPrice : BaseEntity
	{
		public virtual string FiscalYear { get; set; } //FY 2077 or 2077 or All
		public virtual char Alphabet { get; set; }
		public virtual int Value { get; set; }
	}

	public class SymbolicPriceConfiguration : EntityConfiguration<SymbolicPrice>
	{
		public override void ConfigureEntity(EntityTypeBuilder<SymbolicPrice> builder)
		{
			builder.ToTable(nameof(SymbolicPrice));
			builder.Property(c => c.FiscalYear).IsRequired();
			builder.Property(c => c.Alphabet).IsRequired();
			builder.Property(c => c.Value).IsRequired();

			base.Configure(builder);
		}
	}
}
