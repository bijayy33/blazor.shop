﻿using System.Collections.Generic;

using Blazor.Shop.DependencyInjection.Common.Enums;
using Blazor.Shop.DependencyInjection.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blazor.Shop.DependencyInjection.Models
{
	public class Product : BaseEntity
	{
		/// <summary>
		/// General Name of product for people i.e. Petrol Gulf
		/// </summary>
		public virtual string Name { get; set; }

		/// <summary>
		/// Product Code i.e. Part Number (CGPOB-7026)
		/// </summary>
		/// 
		public virtual string Code { get; set; }

		/// <summary>
		/// Product Description i.e. PrtrolGulf-7026
		/// </summary>
		public virtual string Description { get; set; }

		public virtual string Size { get; set; } //Product Size
		public virtual string EncodedMrp { get; set; } //Symbolic MRP or direct value
		public virtual string EncodedCostPrice { get; set; } //Symbolic Cost Price or direct value
		public virtual string EncodedSellingPrice { get; set; } //Symbolic SP or direct value
		public virtual int AvailableQuantity { get; set; }
		public virtual QuantityType QuantityType { get; set; }
		/// <summary>
		/// Quantity Type is Set then it is set of quantity.
		/// </summary>
		public virtual int QuantityTypeValue { get; set; } = 1;
		public virtual ICollection<SoldProduct> SoldProducts { get; set; }
		public virtual ICollection<ProductVat> ProductVats { get; set; }
	}

	public class ProductConfiguration : EntityConfiguration<Product>
	{
		public override void ConfigureEntity(EntityTypeBuilder<Product> builder)
		{
			builder.ToTable(nameof(Product));
			builder.Property(c => c.Name).IsRequired(false);
			builder.Property(c => c.Code).IsRequired(true);
			builder.Property(c => c.Description).IsRequired(false);
			builder.Property(c => c.Size).IsRequired(false);
			builder.Property(c => c.EncodedMrp).IsRequired();
			builder.Property(c => c.EncodedCostPrice).IsRequired();
			builder.Property(c => c.EncodedSellingPrice).IsRequired(false);
			builder.Property(c => c.AvailableQuantity).IsRequired();
			builder.Property(c => c.QuantityType).HasDefaultValue(QuantityType.PCS).IsRequired();
			builder.Property(c => c.QuantityTypeValue).HasDefaultValue(1).IsRequired();

			base.Configure(builder);
		}
	}
}
