﻿
using Blazor.Shop.DependencyInjection.Common.Enums;
using Blazor.Shop.DependencyInjection.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blazor.Shop.DependencyInjection.Models
{
	public class ProductVat : BaseEntity
	{
		public virtual int Quantity { get; set; } //Product Quanties
		public virtual QuantityType QuantityType { get; set; }
		public virtual decimal Rate { get; set; }
		public virtual decimal DiscountAmount { get; set; }
		public virtual decimal Amount { get; set; }
		public virtual long InvoiceNumber { get; set; }
		public virtual Vat Vat { get; set; }
		public virtual string ProductId { get; set; }
		public virtual Product Product { get; set; }
	}

	public class ProductVatConfiguration : EntityConfiguration<ProductVat>
	{
		public override void ConfigureEntity(EntityTypeBuilder<ProductVat> builder)
		{
			builder.ToTable(nameof(ProductVat));
			builder.Property(c => c.ProductId).IsRequired();
			builder.Property(c => c.Quantity).IsRequired();
			builder.Property(c => c.QuantityType).IsRequired();
			builder.Property(c => c.Rate).IsRequired();
			builder.Property(c => c.DiscountAmount).IsRequired(false);
			builder.Property(c => c.Amount).HasDefaultValue(0).IsRequired(false);

			builder.HasOne(x => x.Vat)
				.WithMany(x => x.ProductVats)
				.HasForeignKey(x => x.InvoiceNumber);

			builder.HasOne(x => x.Product)
				.WithMany(x => x.ProductVats)
				.HasForeignKey(x => x.ProductId);

			base.Configure(builder);
		}
	}
}
