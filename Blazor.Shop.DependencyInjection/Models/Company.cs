﻿using System.Collections.Generic;
using Blazor.Shop.DependencyInjection.Common.Enums;
using Blazor.Shop.DependencyInjection.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blazor.Shop.DependencyInjection.Models
{
	public class Company : BaseEntity
	{
		public virtual string Name { get; set; }
		public virtual string TaxTypeNumber { get; set; } //VAT or PAN
		public virtual TaxType TaxType { get; set; }
		public virtual CompanyType Type { get; set; }
		public virtual string Description { get; set; } //John Deer, Sonalika, Gulf, etc
		public virtual string Address { get; set; }
		public virtual string Emails { get; set; }
		public virtual string Phones { get; set; }
		public virtual List<CompanyVat> CompanyVats { get; set; }
	}

	public class CompanyConfiguration : EntityConfiguration<Company>
	{
		public override void ConfigureEntity(EntityTypeBuilder<Company> builder)
		{
			builder.ToTable(nameof(Company));
			builder.Property(c => c.Name).IsRequired();
			builder.Property(c => c.TaxType).IsRequired();
			builder.HasIndex(c => c.TaxTypeNumber).IsUnique();
			builder.Property(c => c.Type).HasDefaultValue(CompanyType.Distributer).IsRequired();
			builder.Property(c => c.Description).IsRequired();
			builder.Property(c => c.Address).IsRequired();
			builder.Property(c => c.Emails).IsRequired(false);
			builder.Property(c => c.Phones).IsRequired();

			base.Configure(builder);
		}
	}
}
