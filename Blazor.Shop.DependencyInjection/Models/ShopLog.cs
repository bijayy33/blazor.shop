﻿using Blazor.Shop.DependencyInjection.Common.Enums;
using Blazor.Shop.DependencyInjection.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blazor.Shop.DependencyInjection.Models
{
	public class ShopLog : BaseEntity
	{
		//Product Id sold by Ajay at price 20 at discount
		//User login's at 12th June at 2 pm.
		public virtual string Details { get; set; }
		public virtual LogFor For { get; set; }
	}

	public class ShopLogConfiguration : EntityConfiguration<ShopLog>
	{
		public override void ConfigureEntity(EntityTypeBuilder<ShopLog> builder)
		{
			builder.ToTable(nameof(ShopLog));
			builder.Property(c => c.Details).IsRequired();
			builder.Property(c => c.For).HasDefaultValue(LogFor.User).IsRequired();

			base.Configure(builder);
		}
	}
}
