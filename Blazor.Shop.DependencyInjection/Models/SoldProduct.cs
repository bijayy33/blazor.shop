﻿
using Blazor.Shop.DependencyInjection.Common.Enums;
using Blazor.Shop.DependencyInjection.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blazor.Shop.DependencyInjection.Models
{
	public class SoldProduct : BaseEntity
	{
		public virtual int AvailableQuantity { get; set; } // Quantity Available when sold 2.
		public virtual int SoldQuantity { get; set; } // Quantity Sold 2.
		public virtual QuantityType QuantityType { get; set; } // Quantity Type.
		public virtual decimal Rate { get; set; }
		public virtual decimal Amount { get; set; }
		public virtual decimal DiscountAmount { get; set; }
		public virtual string SoldId { get; set; } // SoldProductId.
		public virtual Sold Sold { get; set; } // Sold Product.
		public virtual string ProductId { get; set; } // Sold Product.
		public virtual Product Product { get; set; } // Sold Product.
	}

	public class SoldProductInfoConfiguration : EntityConfiguration<SoldProduct>
	{
		public override void ConfigureEntity(EntityTypeBuilder<SoldProduct> builder)
		{
			builder.ToTable(nameof(SoldProduct));
			builder.Property(c => c.AvailableQuantity).IsRequired();
			builder.Property(c => c.SoldQuantity).IsRequired();
			builder.Property(c => c.Rate).IsRequired();
			builder.Property(c => c.Amount).IsRequired();
			builder.Property(c => c.DiscountAmount).HasDefaultValue(0).IsRequired();

			builder.HasOne(x => x.Sold)
				.WithMany(x => x.SoldProducts) //Inside Sold Entity
				.HasForeignKey(x => x.SoldId);

			builder.HasOne(x => x.Product)
				.WithMany(x => x.SoldProducts) //Inside Product Entity
				.HasForeignKey(x => x.ProductId);

			base.Configure(builder);
		}
	}
}
