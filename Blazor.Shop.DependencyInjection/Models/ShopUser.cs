﻿using System;

using Blazor.Shop.DependencyInjection.Common.Enums;

using Microsoft.AspNetCore.Identity;

namespace Blazor.Shop.DependencyInjection.Models
{
	public class ShopUser : IdentityUser
	{
		public string FullName { get; set; }
		public string Address { get; set; }
		public virtual Gender Gender { get; set; }
		public virtual string DefaultLanguage { get; set; }
		public virtual bool IsDeleted { get; set; } = false;
		public virtual DateTimeOffset? CreationDate { get; set; } = DateTime.UtcNow;
		public virtual string CreatedBy { get; set; }
	}
}
