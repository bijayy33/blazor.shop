﻿namespace Blazor.Shop.DependencyInjection.Common.Enums
{
	public enum QuantityType
	{
		PCS = 1,
		SET = 2
	}
}
