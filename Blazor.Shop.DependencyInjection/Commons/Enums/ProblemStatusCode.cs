﻿namespace Blazor.Shop.DependencyInjection.Commons.Enums
{
	public enum ProblemStatusCode
	{
		Error = -1,
		Success = 0,
		Warning = 1
	}
}
