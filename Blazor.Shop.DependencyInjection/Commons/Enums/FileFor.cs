﻿namespace Blazor.Shop.DependencyInjection.Common.Enums
{
	public enum FileFor
	{
		User = 0,
		Product = 1
	}
}
