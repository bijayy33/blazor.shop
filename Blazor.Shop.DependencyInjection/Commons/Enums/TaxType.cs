﻿namespace Blazor.Shop.DependencyInjection.Common.Enums
{
	public enum TaxType
	{
		PAN = 1,
		VAT = 2
	}
}
