﻿namespace Blazor.Shop.DependencyInjection.Common.Enums
{
	public enum LogFor
	{
		User = 1,
		Product = 2,
	}
}
