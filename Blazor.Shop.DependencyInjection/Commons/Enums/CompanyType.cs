﻿namespace Blazor.Shop.DependencyInjection.Common.Enums
{
	public enum CompanyType
	{
		///<summary>Company which manufactures</summary>
		Manufacturer = 1,

		///<summary>Company which is directly connected to manufacturer</summary>
		Distributer = 2,

		///<summary>Directly connected to distributers</summary>
		Dealer = 3,

		///<summary>Sells porduts in bulks to retailer and may be manufacturer itself</summary>
		WholeSaler = 4,

		///<summary>Company which Sells to retailer</summary>
		Reseller = 5,

		///<summary>Last in the chain which sells product one piece or more</summary>
		Retailer = 6
	}
}
