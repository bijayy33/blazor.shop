﻿namespace Blazor.Shop.DependencyInjection.Common.Enums
{
	public enum ActionType
	{
		Fetch = 1,
		FetchAll = 2,
		Add = 3,
		Edit = 4,
		Delete = 5,
		Restore = 6,
		None = 7
	}
}
