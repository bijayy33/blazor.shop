﻿namespace Blazor.Shop.DependencyInjection.Common.Constants
{
	public class DefaultProduct
	{
		//Prevent Duplicate entry by predefining the ids
		public const string ProductId1 = "33123EC-DE36-4373-BD2F-67E73DEEC851";
		public const string ProductId2 = "1A71FB8E-BB35-432B-B614-A76195F300F2";
		public const string ProductId3 = "0BB2E3BE-5BE0-4E51-9AA4-D5592288BF93";
		public const string ProductId4 = "A2E62672-247C-4D6E-B398-2B171963E394";
	}
}
