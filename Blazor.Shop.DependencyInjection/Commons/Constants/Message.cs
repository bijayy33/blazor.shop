﻿namespace Blazor.Shop.DependencyInjection.Commons.Constants
{
	public class Message
	{
		public const string Exists = "Already Exists. Please try another.";
	}
}
