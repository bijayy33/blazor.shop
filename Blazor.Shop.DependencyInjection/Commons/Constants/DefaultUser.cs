﻿namespace Blazor.Shop.DependencyInjection.Common.Constants
{
	public class DefaultUser
	{
		//Prevent Duplicate entry by predefining the ids
		public const string SuperAdminId = "1A71FB8E-BB35-432B-B614-A76195F300AA";
		public const string AdminId = "1A71FB8E-BB35-432B-B614-A76195F300FF";
		public const string SubAdminId = "0BB2E3BE-5BE0-4E51-9AA4-D5592288BF9B";
		public const string ManagerId = "A2E62672-247C-4D6E-B398-2B171963E397";
		public const string UserId = "92CF5C4B-36B1-4D4A-9CCA-C21017225BC8";
		public const string DistributerId = "1A71FB8E-BB35-432B-B614-A76195F300FE";
		public const string ResellerId = "A2E62672-247C-4D6E-B398-2B171963E392";
		public const string CompanyId = "92CF5C4B-36B1-4D4A-9CCA-C21017225BC3";

		//UserName and Email
		public const string SuperAdminEmail = "superadmin@shop.com";
		public const string AdminEmail = "admin@shop.com";
		public const string SubAdminEmail = "subadmin@shop.com";
		public const string ManagerEmail = "manager@shop.com";
		public const string UserEmail = "user@shop.com";
		public const string DistributerEmail = "distributer@shop.com";
		public const string ResellerEmail = "reseller@shop.com";
		public const string CompanyEmail = "company@shop.com";
		public const string Password = "1234";
	}
}
