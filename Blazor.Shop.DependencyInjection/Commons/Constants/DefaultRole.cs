﻿namespace Blazor.Shop.DependencyInjection.Common.Constants
{
	public class DefaultRole
	{
		//Default Roles Ids
		public const string SuperAdminRoleId = "CFD05CE8-DEE4-443E-9951-F43926F4546B";
		public const string AdminRoleId = "94FFDD19-A434-4DF1-AB07-8D5F89B774B2";
		public const string SubAdminRoleId = "86EB3F7D-ACD4-4A22-A798-A903B10EE17A";
		public const string ManagerRoleId = "A4BA9887-EE62-4BA2-8E44-49335C2B7F23";
		public const string UserRoleId = "638D5874-2FD3-4299-9487-7E2DC7F78ADB";
		public const string DistributerRoleId = "638D5874-2FD3-4299-9487-7E2DC7F78AD0";
		public const string ResellerRoleId = "638D5874-2FD3-4299-9487-7E2DC7F78AD1";
		public const string CompanyRoleId = "638D5874-2FD3-4299-9487-7E2DC7F78AD2";

		//Default Roles
		public const string SuperAdmin = nameof(SuperAdmin);
		public const string Seller = nameof(Seller);
		public const string Buyer = nameof(Buyer);
	}
}
