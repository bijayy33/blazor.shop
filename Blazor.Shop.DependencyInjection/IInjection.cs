﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace Blazor.Shop.DependencyInjection
{
	public interface IInjection
	{
		Task Inject(IServiceCollection services, IConfiguration configuration);
	}
}
