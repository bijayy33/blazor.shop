﻿using System;
using System.Linq;
using System.Runtime.Loader;

namespace Blazor.Shop.DependencyInjection.Resolvers
{
	public class CustomActivator
	{
		public IInterface GetInstance<IInterface>(params object?[]? parameters)
		{
			IInterface instance = (IInterface)Activator.CreateInstance(this.GetImplementations<IInterface>(), parameters);
			return instance;
		}

		public Type GetImplementations<IInterface>()
		{
			Type implementation = AssemblyLoadContext.Default.Assemblies.SelectMany(assembly => assembly.GetTypes())
				.FirstOrDefault(type => typeof(IInterface).IsAssignableFrom(type)
											 && !type.IsInterface
											 && !type.IsAbstract);

			return implementation;
		}
	}
}
