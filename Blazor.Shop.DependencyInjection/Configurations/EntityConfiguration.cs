﻿
using System;

using Blazor.Shop.DependencyInjection.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blazor.Shop.DependencyInjection.Configurations
{
	public abstract class EntityConfiguration<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : BaseEntity
	{
		public void Configure(EntityTypeBuilder<TEntity> builder)
		{
			builder.Property(c => c.Id).ValueGeneratedOnAdd().Metadata.IsPrimaryKey();
			builder.Property(c => c.IsDeleted).HasDefaultValue(false).IsRequired();
			builder.Property(c => c.CreatedById).IsRequired(false);
			builder.Property(c => c.CreationDate).ValueGeneratedOnAdd().HasDefaultValue(DateTimeOffset.Now).IsRequired(false);
			builder.Property(c => c.ChangedById).IsRequired(false);
			builder.Property(c => c.ChangedDate).ValueGeneratedOnUpdate().HasDefaultValue(DateTimeOffset.Now).IsRequired(false);
		}

		public abstract void ConfigureEntity(EntityTypeBuilder<TEntity> builder);
	}
}
